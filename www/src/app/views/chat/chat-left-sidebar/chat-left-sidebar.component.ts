import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/shared/services/auth.service';
import { ChatService } from '../chat.service';

@Component({
  selector: 'app-chat-left-sidebar',
  templateUrl: './chat-left-sidebar.component.html',
  styleUrls: ['./chat-left-sidebar.component.scss']
})
export class ChatLeftSidebarComponent implements OnInit {
  contacts: any[];
  empresa: number;

  constructor(
    private chatService: ChatService,
    private auth: AuthService
  ) {}

  ngOnInit() {
    this.empresa = this.auth.getuser();
    this.chatService.getContacts(this.empresa).subscribe(results =>{
      this.contacts = results;
    })
  }
  

  getChatByContact(contactId) {
    this.chatService.getChatByContact(contactId);
  }
}
