import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject, Subject, of, combineLatest } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { HttpService } from 'src/app/shared/services/http.service';

export interface Chat {
  text: string;
  time: Date | string;
  contactId: User['id'];
}

export interface ChatCollection {
  id: string;
  chats: Chat[];
}

export interface UserChatInfo {
  chatId: ChatCollection['id'];
  contactId: User['id'];
  contactName: User['nombre'];
  unread: number;
  lastChatTime: Date | string;
}

export class User {
  id: string;
  avatar: string;
  nombre: string;
  chatInfo?: UserChatInfo[];
}

@Injectable({
  providedIn: 'root'
})
export class ChatService {
  public contacts: User[];
  public chats: ChatCollection[];
  public user: User;

  onContactSelected = new BehaviorSubject<any>(null);
  onUserUpdated = new Subject<User>();
  onChatSelected = new BehaviorSubject<any>(null);
  onChatsUpdated = new Subject<any>();
  loadingCollection: boolean;

  constructor(
    private http: HttpClient,
    private httpService: HttpService
  ) {

  }

  getContacts(empresa: number): Observable<any[]>{
    return this.http.get<any[]>(
      this.httpService.getBaseUrl() + '/usuarios/mensajes/empresa/' + empresa.toString(),
      { headers: this.httpService.getHeaders() }
    );
  }

  
  getChatByContact(contactId) {
    this.loadingCollection = true;
  }


}

