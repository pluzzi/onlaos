import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class MensajesService {

  constructor(
    private http: HttpService,
    private httpClient: HttpClient,
  ) { }

  getNotifications(empresa: number): Observable<any>{
    return this.httpClient.get<any>(
      this.http.getBaseUrl() + '/mensajes/notificaciones/empresa/' + empresa.toString(),
      { headers: this.http.getHeaders() }
    );
  }
}
