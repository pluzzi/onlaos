import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  private baseUrl = 'http://onlaos.com.ar/apis';

  constructor() { }

  public getBaseUrl(): string{
    return this.baseUrl;
  }

  public getHeaders(): HttpHeaders {
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    console.log(headers);
    return headers;
  }
  
}
