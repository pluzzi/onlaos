<?php
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

require 'src/config/database-config.php';
require 'vendor/autoload.php';

$app = new \Slim\App();

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
});

$logger = $app->getLog();

/**
 * @param bool $displayErrorDetails -> Should be set to false in production
 * @param bool $logErrors -> Parameter is passed to the default ErrorHandler
 * @param bool $logErrorDetails -> Display error details in error log
 * which can be replaced by a callable of your choice.
 * @param \Psr\Log\LoggerInterface $logger -> Optional PSR-3 logger to receive errors
 * 
 * Note: This middleware should be added last. It will not handle any exceptions/errors
 * for middleware added after it.
 */
$errorMiddleware = $app->addErrorMiddleware(true, true, true, $logger);

require 'src/rutas/usuarios.php';
require 'src/rutas/noticias.php';
require 'src/rutas/agenda.php';
require 'src/rutas/programas.php';
require 'src/rutas/vivo.php';
require 'src/rutas/productos.php';
require 'src/rutas/rubros.php';
require 'src/rutas/empresas.php';
require 'src/rutas/mensajes.php';

$app->run();