<?php
    class database{
        public function getConnection(){
            $options = [
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_CASE => PDO::CASE_NATURAL,
                PDO::ATTR_ORACLE_NULLS => PDO::NULL_EMPTY_STRING
            ];
            
            $conn = new PDO('mysql:host=sql155.main-hosting.eu;dbname=u481693956_base', 'u481693956_user', 'onlaos', $options);

            return $conn;
        }

    }