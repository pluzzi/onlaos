<?php
    use Psr\Http\Message\ServerRequestInterface as Request;
    use Psr\Http\Message\ResponseInterface as Response;

    $app->get('/noticias/{page}', function (Request $request, Response $response, array $args) {
        $page = $args['page'];
        $limit = $page * 3;
        
        $sql = "select
                    idNovedad, titulo, copete, cuerpo, fechaPublicacion, foto
                from novedades
                where publicado = 1
                order by idNovedad desc
                limit ".$limit.",3";
        $db_config = new database();
        $conn = $db_config->getConnection();
        $result = $conn->query($sql);

        if($result->rowCount() > 0){
            $rows = array();
            while($row = $result->fetch(PDO::FETCH_ASSOC)){
                $item = array();
                $item['idNovedad'] = $row['idNovedad'];
                $item['titulo'] = $row['titulo'];
                $item['copete'] = $row['copete'];
                $item['cuerpo'] = $row['cuerpo'];
                $item['fechaPublicacion'] = $row['fechaPublicacion'];
                $item['foto'] = "data:image/jpeg;base64,".base64_encode($row['foto']);
                array_push($rows, $item);
            }
            return json_encode($rows);
        }else{
            $item = array();
            $item["code"] = 0;
            $item["message"] = "No existen regstros.";
            return json_encode($item);
        }

    });

    $app->get('/noticias/id/{id}', function (Request $request, Response $response, array $args) {
        $sql = "select
                    idNovedad, titulo, copete, cuerpo, fechaPublicacion, foto
                from novedades
                where idNovedad = ". $args["id"];

        $db_config = new database();
        $conn = $db_config->getConnection();
        $result = $conn->query($sql);

        if($result->rowCount() > 0){
            $rows = array();
            while($row = $result->fetch(PDO::FETCH_ASSOC)){
                $item = array();
                $item['idNovedad'] = $row['idNovedad'];
                $item['titulo'] = $row['titulo'];
                $item['copete'] = $row['copete'];
                $item['cuerpo'] = $row['cuerpo'];
                $item['fechaPublicacion'] = $row['fechaPublicacion'];
                $item['foto'] = "data:image/jpeg;base64,".base64_encode($row['foto']);
                array_push($rows, $item);
            }
            return json_encode($rows);
        }else{
            $item = array();
            $item["code"] = 0;
            $item["message"] = "No existen regstros.";
            return json_encode($item);
        }

    });

?>