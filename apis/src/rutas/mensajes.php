<?php
    use Psr\Http\Message\ServerRequestInterface as Request;
    use Psr\Http\Message\ResponseInterface as Response;

    $app->get('/mensajes/{usuario}/{empresa}', function (Request $request, Response $response, array $args) {
        $usuario = $args['usuario'];
        $empresa = $args['empresa'];
        
        $sql = "select
                    id, usuario, empresa, tipo, mensaje, fecha
                from mensajes
                where usuario = ".$usuario."
                and empresa = ".$empresa."
                order by id asc";

        $db_config = new database();
        $conn = $db_config->getConnection();
        $result = $conn->query($sql);

        if($result->rowCount() > 0){
            $rows = array();
            while($row = $result->fetch(PDO::FETCH_ASSOC)){
                $item = array();
                $item['id'] = $row['id'];
                $item['usuario'] = $row['usuario'];
                $item['empresa'] = $row['empresa'];
                $item['tipo'] = $row['tipo'];
                $item['mensaje'] = $row['mensaje'];
                $item['fecha'] = $row['fecha'];
                array_push($rows, $item);
            }
            return json_encode($rows);
        }else{
            $item = array();
            $item["code"] = 0;
            $item["message"] = "No existen regstros.";
            return json_encode($item);
        }

    });

    $app->post('/mensajes/send', function (Request $request, Response $response, array $args) {
        $body = $request->getParsedBody();
        $usuario = $body['usuario'];
        $empresa = $body['empresa'];
        $tipo = $body['tipo'];
        $mensaje = $body['mensaje'].trim();

        if($mensaje==""){
            $item = array();
            $item["code"] = 0;
            $item["message"] = "Debe ingresar un texto.";
            return json_encode($item);
        }
        
        $sql = "insert into mensajes(usuario, empresa, tipo, mensaje, fecha)
                values(".$usuario.",".$empresa.",".$tipo.",'".$mensaje."', NOW())";

        $db_config = new database();
        $conn = $db_config->getConnection();
        $result = $conn->query($sql);

        if($result->rowCount() > 0){
            $item = array();
            $item["code"] = 1;
            $item["message"] = "OK.";
            return json_encode($item);
        }else{
            $item = array();
            $item["code"] = 0;
            $item["message"] = "Se producjo un error.";
            return json_encode($item);
        }

    });

    $app->get('/mensajes/notificaciones/empresa/{empresa}', function (Request $request, Response $response, array $args) {
        $empresa = $args['empresa'];
        
        $sql = "select
                    id, usuario, empresa, tipo, mensaje, fecha
                from mensajes
                where empresa = ".$empresa."
                and visto = 0
                order by id asc";

        $db_config = new database();
        $conn = $db_config->getConnection();
        $result = $conn->query($sql);

        if($result->rowCount() > 0){
            $rows = array();
            while($row = $result->fetch(PDO::FETCH_ASSOC)){
                $item = array();
                $item['id'] = $row['id'];
                $item['icon'] = 'i-Speach-Bubble-6';
                $item['title'] = 'Nuevo mensaje';
                $item['badge'] = '1';
                $item['text'] = $row['mensaje'];
                $item['time'] = $row['fecha'];
                $item['status'] = 'primary';
                $item['link'] = '/chat';
                array_push($rows, $item);
            }
            return json_encode($rows);
        }else{
            $item = array();
            $item["code"] = 0;
            $item["message"] = "No existen regstros.";
            return json_encode($item);
        }

    });

?>