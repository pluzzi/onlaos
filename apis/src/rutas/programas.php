<?php
    use Psr\Http\Message\ServerRequestInterface as Request;
    use Psr\Http\Message\ResponseInterface as Response;

    $app->get('/programas/{page}', function (Request $request, Response $response, array $args) {
        $page = $args['page'];
        $limit = $page * 24;
        
        $sql = "select 
                    idPrograma, idCategoriaVideo, programa, bloque, titulo, codigo, fecha, entrevistado, informe, etiquetas 
                from programas
                where publicado = 1 
                order by idPrograma desc
                limit ".$limit.",24";

        $db_config = new database();
        $conn = $db_config->getConnection();
        $result = $conn->query($sql);

        if($result->rowCount() > 0){
            $rows = array();
            while($row = $result->fetch(PDO::FETCH_ASSOC)){
                $item = array();
                $item['idPrograma'] = $row['idPrograma'];
                $item['idCategoriaVideo'] = $row['idCategoriaVideo'];
                $item['programa'] = $row['programa'];
                $item['bloque'] = $row['bloque'];
                $item['titulo'] = $row['titulo'];
                $item['codigo'] = $row['codigo'];
                $item['img'] = "http://img.youtube.com/vi/".$row['codigo']."/0.jpg";
                $item['url'] = "https://www.youtube.com/embed/".$row['codigo'];
                $item['fecha'] = $row['fecha'];
                $item['entrevistado'] = $row['entrevistado'];
                $item['informe'] = $row['informe'];
                $item['etiquetas'] = $row['etiquetas'];
                array_push($rows, $item);
            }
            return json_encode($rows);
        }else{
            $item = array();
            $item["code"] = 0;
            $item["message"] = "No existen regstros.";
            return json_encode($item);
        }

    });

    $app->get('/programas/id/{id}', function (Request $request, Response $response, array $args) {
        $id = $args['id'];
        
        $sql = "select 
                    idPrograma, idCategoriaVideo, programa, bloque, titulo, codigo, fecha, entrevistado, informe, etiquetas 
                from programas
                where idprograma = ".$id;

        $db_config = new database();
        $conn = $db_config->getConnection();
        $result = $conn->query($sql);

        if($result->rowCount() > 0){
            $rows = array();
            while($row = $result->fetch(PDO::FETCH_ASSOC)){
                $item = array();
                $item['idPrograma'] = $row['idPrograma'];
                $item['idCategoriaVideo'] = $row['idCategoriaVideo'];
                $item['programa'] = $row['programa'];
                $item['bloque'] = $row['bloque'];
                $item['titulo'] = $row['titulo'];
                $item['codigo'] = $row['codigo'];
                $item['img'] = "http://img.youtube.com/vi/".$row['codigo']."/0.jpg";
                $item['url'] = "https://www.youtube.com/embed/".$row['codigo']."?autoplay=1&fs=1&enablejsapi=1&enablecastapi=1";
                $item['fecha'] = $row['fecha'];
                $item['entrevistado'] = $row['entrevistado'];
                $item['informe'] = $row['informe'];
                $item['etiquetas'] = $row['etiquetas'];
                array_push($rows, $item);
            }
            return json_encode($rows);
        }else{
            $item = array();
            $item["code"] = 0;
            $item["message"] = "No existen regstros.";
            return json_encode($item);
        }

    });

?>