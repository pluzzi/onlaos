<?php
    use Psr\Http\Message\ServerRequestInterface as Request;
    use Psr\Http\Message\ResponseInterface as Response;

    $app->get('/productos/{page}', function (Request $request, Response $response, array $args) {
        $page = $args['page'];
        $limit = $page * 24;
        
        $sql = "select
                id, nombre, logo
                from servicios_empresas
                order by id desc
                limit ".$limit.",24";
        $db_config = new database();
        $conn = $db_config->getConnection();
        $result = $conn->query($sql);

        if($result->rowCount() > 0){
            $rows = array();
            while($row = $result->fetch(PDO::FETCH_ASSOC)){
                $item = array();
                $item['id'] = $row['id'];
                $item['nombre'] = $row['nombre'];
                $item['logo'] = "data:image/jpeg;base64,".base64_encode($row['logo']);
                array_push($rows, $item);
            }
            return json_encode($rows);
        }else{
            $item = array();
            $item["code"] = 0;
            $item["message"] = "No existen regstros.";
            return json_encode($item);
        }

    });

    $app->get('/productos/id/{id}', function (Request $request, Response $response, array $args) {
        $sql = "select
                id, nombre, logo
                from servicios_empresas
                where id = ". $args["id"];

        $db_config = new database();
        $conn = $db_config->getConnection();
        $result = $conn->query($sql);

        if($result->rowCount() > 0){
            $rows = array();
            while($row = $result->fetch(PDO::FETCH_ASSOC)){
                $item = array();
                $item['id'] = $row['id'];
                $item['nombre'] = $row['nombre'];
                $item['logo'] = "data:image/jpeg;base64,".base64_encode($row['logo']);
                array_push($rows, $item);
            }
            return json_encode($rows);
        }else{
            $item = array();
            $item["code"] = 0;
            $item["message"] = "No existen regstros.";
            return json_encode($item);
        }

    });

?>