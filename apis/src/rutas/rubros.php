<?php
    use Psr\Http\Message\ServerRequestInterface as Request;
    use Psr\Http\Message\ResponseInterface as Response;

    $app->get('/rubros', function (Request $request, Response $response, array $args) {
        $sql = "select
                idRubro, nombre
                from rubros
                order by nombre asc";
        $db_config = new database();
        $conn = $db_config->getConnection();
        $result = $conn->query($sql);

        if($result->rowCount() > 0){
            $rows = array();
            while($row = $result->fetch(PDO::FETCH_ASSOC)){
                $item = array();
                $item['idRubro'] = $row['idRubro'];
                $item['nombre'] = $row['nombre'];
                array_push($rows, $item);
            }
            return json_encode($rows);
        }else{
            $item = array();
            $item["code"] = 0;
            $item["message"] = "No existen regstros.";
            return json_encode($item);
        }

    });

?>