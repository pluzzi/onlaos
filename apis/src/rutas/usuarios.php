<?php
    use Psr\Http\Message\ServerRequestInterface as Request;
    use Psr\Http\Message\ResponseInterface as Response;

    $app->post('/usuarios/login', function (Request $request, Response $response, array $args) {
        $body = $request->getParsedBody();
        $email = $body['email'];
        $password = $body['password'];
        $sql = "select id, nombre, email from app_usuarios where email='".$email."' and password='".$password."'";
        $db_config = new database();
        $conn = $db_config->getConnection();
        $result = $conn->query($sql);

        if($result->rowCount() > 0){
            $row = $result->fetch(PDO::FETCH_ASSOC);
            return json_encode($row);;
        }else{
            $item = array();
            $item["code"] = 0;
            $item["message"] = "E-Mail o Password incorrectos.";
            return json_encode($item);
        }

    });

    $app->post('/usuarios/register', function (Request $request, Response $response, array $args) {
        $body = $request->getParsedBody();
        $nombre = $body['nombre'];
        $email = $body['email'];
        $password = $body['password'];

        $sql = "select 1 from app_usuarios where email='".$email."'";
        $db_config = new database();
        $conn = $db_config->getConnection();
        $result = $conn->query($sql);

        if($result->rowCount() > 0){
            $item = array();
            $item["code"] = 0;
            $item["message"] = "El E-Mail ya esta en uso.";
            return json_encode($item);
        }

        $sql = "insert into app_usuarios(nombre, email, password) values('".$nombre."','".$email."','".$password."')";
        $result = $conn->query($sql);

        if($result->rowCount() > 0){
            $item = array();
            $item["code"] = 1;
            $item["message"] = "OK.";
            return json_encode($item);
        }else{
            $item = array();
            $item["code"] = 0;
            $item["message"] = "E-Mail o Password incorrectos.";
            return json_encode($item);
        }

    });

    $app->get('/usuarios/mensajes/empresa/{empresa}', function (Request $request, Response $response, array $args) {
        $empresa = $args['empresa'];
        
        $sql = "select
                u.id,
                u.nombre,
                u.email,
                u.avatar
                from app_usuarios u
                where (select count(1) from mensajes m where m.usuario = u.id and m.empresa = ".$empresa.") > 0";

        $db_config = new database();
        $conn = $db_config->getConnection();
        $result = $conn->query($sql);

        if($result->rowCount() > 0){
            $rows = array();
            while($row = $result->fetch(PDO::FETCH_ASSOC)){
                $item = array();
                $item['id'] = $row['id'];
                $item['nombre'] = $row['nombre'];
                $item['email'] = $row['email'];
                $item['avatar'] = "data:image/jpeg;base64,".base64_encode($row['avatar']);
                array_push($rows, $item);
            }
            return json_encode($rows);
        }else{
            $item = array();
            $item["code"] = 0;
            $item["message"] = "No existen regstros.";
            return json_encode($item);
        }

    });
?>