import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'init',
    loadChildren: () => import('./pages/init/init.module').then( m => m.InitPageModule)
  },
  {
    path: 'menu',
    loadChildren: () => import('./pages/menu/menu.module').then( m => m.MenuPageModule)
  },
  {
    path: 'rubros',
    loadChildren: () => import('./pages/rubros/rubros.module').then( m => m.RubrosPageModule)
  },
  {
    path: 'empresa',
    loadChildren: () => import('./pages/empresa/empresa.module').then( m => m.EmpresaPageModule)
  },
  {
    path: 'dialogo',
    loadChildren: () => import('./pages/dialogo/dialogo.module').then( m => m.DialogoPageModule)
  },
  {
    path: 'agenda-detalle',
    loadChildren: () => import('./pages/agenda-detalle/agenda-detalle.module').then( m => m.AgendaDetallePageModule)
  },
  {
    path: 'novedad-detalle',
    loadChildren: () => import('./pages/novedad-detalle/novedad-detalle.module').then( m => m.NovedadDetallePageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./pages/register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'info',
    loadChildren: () => import('./pages/info/info.module').then( m => m.InfoPageModule)
  }


];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
