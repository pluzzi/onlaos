export interface Empresa {
    id: number;
    nombre: string;
    logo: string;
    rubro: number;
    facebook: string;
    instagram: string;
}
