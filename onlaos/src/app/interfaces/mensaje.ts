export interface Mensaje {
    id: number;
    usuario: number;
    empresa: number;
    tipo: number,
    mensaje: string;
    fecha: Date;
}
