export interface Programa {
    id: number;
    url: string;
    img?: string;
}
