import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApisService } from './apis.service';

@Injectable({
  providedIn: 'root'
})
export class ProductosService {

  constructor(
    private http: HttpClient,
    private config: ApisService
  ) { }

  getProductos(page: number): Observable<any>{
    return this.http.get<any>(
      this.config.getBaseUrl() + '/productos/' + page.toString(),
      { headers: this.config.getHeaders() }
    )
  }

  getProducto(id: number): Observable<any>{
    return this.http.get<any>(
      this.config.getBaseUrl() + '/producto/id/' + id.toString(),
      { headers: this.config.getHeaders() }
    )
  }
}
