import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApisService } from './apis.service';

@Injectable({
  providedIn: 'root'
})
export class EmpresasService {

  constructor(
    private http: HttpClient,
    private config: ApisService
  ) { }

  getEmpresas(rubro: number, page: number): Observable<any>{
    return this.http.get<any>(
      this.config.getBaseUrl() + '/empresas/'+ rubro.toString() + '/' + page.toString(),
      { headers: this.config.getHeaders() }
    )
  }

  getEmpresa(id: number): Observable<any>{
    return this.http.get<any>(
      this.config.getBaseUrl() + '/empresas/'+ id.toString(),
      { headers: this.config.getHeaders() }
    )
  }
}
