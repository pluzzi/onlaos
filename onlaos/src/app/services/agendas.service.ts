import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApisService } from './apis.service';

@Injectable({
  providedIn: 'root'
})
export class AgendasService {

  constructor(
    private http: HttpClient,
    private config: ApisService
  ) { }

  getAgendas(page: number): Observable<any>{
    return this.http.get<any>(
      this.config.getBaseUrl() + '/agendas/' + page.toString(),
      { headers: this.config.getHeaders() }
    )
  }

  getAgenda(id: number): Observable<any>{
    return this.http.get<any>(
      this.config.getBaseUrl() + '/agendas/id/' + id.toString(),
      { headers: this.config.getHeaders() }
    )
  }
}
