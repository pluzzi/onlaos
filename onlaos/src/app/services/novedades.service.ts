import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApisService } from './apis.service';

@Injectable({
  providedIn: 'root'
})
export class NovedadesService {

  constructor(
    private http: HttpClient,
    private config: ApisService
  ) { }

  getNovedades(page: number): Observable<any>{
    return this.http.get<any>(
      this.config.getBaseUrl() + '/noticias/' + page.toString(),
      { headers: this.config.getHeaders() }
    )
  }

  getNovedad(id: number): Observable<any>{
    return this.http.get<any>(
      this.config.getBaseUrl() + '/noticias/id/' + id.toString(),
      { headers: this.config.getHeaders() }
    )
  }
}
