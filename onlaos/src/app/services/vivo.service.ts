import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApisService } from './apis.service';

@Injectable({
  providedIn: 'root'
})
export class VivoService {

  constructor(
    private http: HttpClient,
    private config: ApisService
  ) { }

  getLastVivo(): Observable<any>{
    return this.http.get<any>(
      this.config.getBaseUrl() + '/vivo',
      { headers: this.config.getHeaders() }
    )
  }
}
