import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { Usuario } from '../intefaces/usuario';
import { ApisService } from './apis.service';

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {

  private usuario: Usuario

  constructor(
    private http: HttpClient,
    private config: ApisService,
    private navCtrl: NavController
  ) { }

  getLogin(email: string, password: string): Observable<Usuario>{
    let usuario = {
      email: email,
      password: password
    }
    return this.http.post<Usuario>(
      this.config.getBaseUrl() + '/usuarios/login',
      usuario,
      { headers: this.config.getHeaders() }
    )
  }

  register(nombre: string, email: string, password: string): Observable<any>{
    let usuario = {
      nombre: nombre,
      email: email,
      password: password
    }
    return this.http.post<Usuario>(
      this.config.getBaseUrl() + '/usuarios/register',
      usuario,
      { headers: this.config.getHeaders() }
    )
  }

  setUsuario(user: Usuario){
    this.usuario = user;
    localStorage.setItem('usuario', JSON.stringify(user));
  }

  getUsuario(): Usuario {
    if(this.usuario){
      return this.usuario;
    }else{
      return JSON.parse(localStorage.getItem('usuario'));
    }
  }

  logout(){
    this.usuario = null;
    localStorage.removeItem('usuario');
    this.navCtrl.navigateRoot(['/']);
  }
}
