import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApisService } from './apis.service';

@Injectable({
  providedIn: 'root'
})
export class RubrosService {

  constructor(
    private http: HttpClient,
    private config: ApisService
  ) { }

  getRubros(): Observable<any>{
    return this.http.get<any>(
      this.config.getBaseUrl() + '/rubros',
      { headers: this.config.getHeaders() }
    )
  }
}
