import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApisService {

  private baseUrl = 'http://onlaos.com.ar/apis';
  
  constructor() {

  }

  public getBaseUrl(): string{
    return this.baseUrl;
  }

  public getHeaders(): HttpHeaders {
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json; charset=utf-8');
    headers.append('Accept', 'application/json');
    return headers;
  }
}
