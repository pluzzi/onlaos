import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApisService } from './apis.service';
import { UsuariosService } from './usuarios.service';

@Injectable({
  providedIn: 'root'
})
export class MensajesService {

  constructor(
    private http: HttpClient,
    private config: ApisService,
    private usuarioService: UsuariosService
  ) { }

  getMensajes(empresa: number): Observable<any>{
    var user = this.usuarioService.getUsuario()

    return this.http.get<any>(
      this.config.getBaseUrl() + '/mensajes/' + user.id.toString() + '/' + empresa.toString(),
      { headers: this.config.getHeaders() }
    )
  }

  sendMensaje(empresa: number, mensaje: string): Observable<any>{
    var user = this.usuarioService.getUsuario()

    var data = {
      usuario: user.id,
      empresa: empresa,
      tipo: 2,
      mensaje: mensaje
    }
    return this.http.post<any>(
      this.config.getBaseUrl() + '/mensajes/send',
      data,
      { headers: this.config.getHeaders() }
    )
  }
}
