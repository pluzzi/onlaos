import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApisService } from './apis.service';

@Injectable({
  providedIn: 'root'
})
export class ProgramasService {

  constructor(
    private http: HttpClient,
    private config: ApisService
  ) { }

  getProgramas(page: number): Observable<any>{
    return this.http.get<any>(
      this.config.getBaseUrl() + '/programas/' + page.toString(),
      { headers: this.config.getHeaders() }
    )
  }

  getPrograma(id: number): Observable<any>{
    return this.http.get<any>(
      this.config.getBaseUrl() + '/programas/id/' + id.toString(),
      { headers: this.config.getHeaders() }
    )
  }
}
