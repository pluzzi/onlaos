import { TestBed } from '@angular/core/testing';

import { VivoService } from './vivo.service';

describe('VivoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: VivoService = TestBed.get(VivoService);
    expect(service).toBeTruthy();
  });
});
