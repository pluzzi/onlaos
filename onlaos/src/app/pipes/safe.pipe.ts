import { Pipe, PipeTransform } from '@angular/core';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';

@Pipe({
  name: 'safe'
})
export class SafePipe implements PipeTransform {

  constructor(private sanitizer: DomSanitizer){

  }
  transform(value: any, ...args: any[]): any {
    debugger
    if (value==null){
      return
    }
    let url: SafeResourceUrl = this.sanitizer.bypassSecurityTrustResourceUrl(value)

    return url
  }

}
