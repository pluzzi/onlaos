import { Component, OnInit } from '@angular/core';
import { NavController, ToastController } from '@ionic/angular';
import { UsuariosService } from 'src/app/services/usuarios.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  email: string = null
  password: string = null
  error: string
  usuario: any

  constructor(
    private navCtrl: NavController,
    private usuarios: UsuariosService,
    public toastController: ToastController
  ) { }

  ngOnInit() {
    this.usuario = this.usuarios.getUsuario()
    if(this.usuario){
      this.navCtrl.navigateRoot(['/menu/home']);
    }
  }

  onEnter(){
    this.usuarios.getLogin(this.email, this.password).subscribe(
      result => {
        if(result['code'] == 0){
          this.presentToast(result['message'])
        }else{
          this.usuarios.setUsuario(result)
          this.navCtrl.navigateRoot(['/init']);
        }
      },
      error => {
        console.log(error)
        //this.presentToast(JSON.stringify(error))
        this.error = JSON.stringify(error)
      }
    )
  }

  goToRegister(){
    this.navCtrl.navigateRoot(['/register']);
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

}
