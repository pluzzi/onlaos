import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { Empresa } from 'src/app/interfaces/empresa';
import { Mensaje } from 'src/app/interfaces/mensaje';
import { EmpresasService } from 'src/app/services/empresas.service';
import { MensajesService } from 'src/app/services/mensajes.service';

@Component({
  selector: 'app-dialogo',
  templateUrl: './dialogo.page.html',
  styleUrls: ['./dialogo.page.scss'],
})
export class DialogoPage implements OnInit {
  id: number;
  empresa: Empresa;
  mensajes: Mensaje[];
  mensaje: string;

  @ViewChild('list', { static: false} ) private list: any;
  
  constructor(
    private route: ActivatedRoute,
    private empresaService: EmpresasService,
    private mensajesService: MensajesService,
    public toastController: ToastController
  ) {
    this.route.params.subscribe(params => {
      this.id = params['id']
      this.empresaService.getEmpresa(this.id).subscribe(result =>{
        this.empresa = result[0]
      })
      this.getMensajes()

    });
  }

  ngOnInit() {
  }

  getMensajes(){
    this.mensajesService.getMensajes(this.id).subscribe(result => {
      if(result.code == undefined){
        this.mensajes = result
      }
    })
  }

  send(){
    this.mensajesService.sendMensaje(this.id, this.mensaje).subscribe(result =>{
      //this.presentToast(result.message)
      this.mensaje = ""
      this.getMensajes()
    })
    
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

}
