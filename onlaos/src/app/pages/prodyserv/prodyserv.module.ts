import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProdyservPageRoutingModule } from './prodyserv-routing.module';

import { ProdyservPage } from './prodyserv.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProdyservPageRoutingModule
  ],
  declarations: [ProdyservPage]
})
export class ProdyservPageModule {}
