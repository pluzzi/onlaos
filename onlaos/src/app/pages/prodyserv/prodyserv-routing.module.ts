import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProdyservPage } from './prodyserv.page';

const routes: Routes = [
  {
    path: '',
    component: ProdyservPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProdyservPageRoutingModule {}
