import { Component, OnInit } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { ProductosService } from 'src/app/services/productos.service';

@Component({
  selector: 'app-prodyserv',
  templateUrl: './prodyserv.page.html',
  styleUrls: ['./prodyserv.page.scss'],
})
export class ProdyservPage implements OnInit {

  productos = new Array()
  page: number = 0
  loading: any

  constructor(
    private prodService: ProductosService,
    private loadingController: LoadingController
  ) { }

  ngOnInit() {
    this.presentLoading();
    this.addMoreProgramas(undefined)
  }

  loadData(event) {
    this.addMoreProgramas(event)
  }

  addMoreProgramas(event){
    this.prodService.getProductos(this.page).subscribe(
      result =>{
        if(this.productos.length != 0){
          if(result.code == undefined){
            result.forEach(element => {
              this.productos.push(element)
            });
          }
        }else{
          this.productos = result
        }
        
        this.loading.dismiss()
        
        if(event!=undefined){
          event.target.complete()
        }
        
        this.page = this.page + 1
      }
    )
  }

  async presentLoading() {
    this.loading = await this.loadingController.create({
      message: 'Cargando datos...',
      duration: 0
    });
    await this.loading.present();
  }

}
