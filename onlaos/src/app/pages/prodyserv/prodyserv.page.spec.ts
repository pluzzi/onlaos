import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ProdyservPage } from './prodyserv.page';

describe('ProdyservPage', () => {
  let component: ProdyservPage;
  let fixture: ComponentFixture<ProdyservPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProdyservPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ProdyservPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
