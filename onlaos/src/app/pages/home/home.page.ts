import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  constructor(
    private navCtrl: NavController
  ) { }

  ngOnInit() {
  }

  goto(id){
    console.log(id);
    
    switch(id){
      case 1:
        this.navCtrl.navigateRoot(['/menu/rubros']);
        break;
      case 2:
        this.navCtrl.navigateRoot(['/menu/envivo']);
        break;
      case 3:
        this.navCtrl.navigateRoot(['/menu/agenda']);
        break;
      case 4:
        this.navCtrl.navigateRoot(['/menu/novedades']);
        break;
      case 5:
        this.navCtrl.navigateRoot(['/menu/programas']);
        break;
      case 6:
        this.navCtrl.navigateRoot(['/menu/prodyserv']);
        break;
    }
  }
}
