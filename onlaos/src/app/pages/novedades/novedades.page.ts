import { Component, OnInit } from '@angular/core';
import { LoadingController, NavController } from '@ionic/angular';
import { NovedadesService } from 'src/app/services/novedades.service';

@Component({
  selector: 'app-novedades',
  templateUrl: './novedades.page.html',
  styleUrls: ['./novedades.page.scss'],
})
export class NovedadesPage implements OnInit {

  novedades = new Array()
  page: number = 0
  loading: any

  constructor(
    private navCtrl: NavController,
    private novService: NovedadesService,
    private loadingController: LoadingController
  ) { }

  ngOnInit() {
    this.presentLoading();
    this.addMoreNovedades(undefined)
  }

  verDetalle(novedad){
    this.navCtrl.navigateRoot(['/menu/novedad-detalle', { id: novedad.idNovedad }]);
  }

  loadData(event) {
    this.addMoreNovedades(event)
  }

  addMoreNovedades(event){
    this.novService.getNovedades(this.page).subscribe(
      result =>{
        if(this.novedades.length != 0){
          if(result.code == undefined){
            result.forEach(element => {
              this.novedades.push(element)
            });
          }
        }else{
          this.novedades = result
        }
        
        this.loading.dismiss()
        
        if(event!=undefined){
          event.target.complete()
        }
        
        this.page = this.page + 1
      }
    )
  }

  async presentLoading() {
    this.loading = await this.loadingController.create({
      message: 'Cargando datos...',
      duration: 0
    });
    await this.loading.present();
  }

}
