import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-init',
  templateUrl: './init.page.html',
  styleUrls: ['./init.page.scss'],
})
export class InitPage implements OnInit {

  constructor(
    private navCtrl: NavController
  ) { }

  ngOnInit() {
    setTimeout( () => { this.navCtrl.navigateRoot(['/menu/home']); }, 2000 );
  }

}
