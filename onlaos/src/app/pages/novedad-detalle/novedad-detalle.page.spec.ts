import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NovedadDetallePage } from './novedad-detalle.page';

describe('NovedadDetallePage', () => {
  let component: NovedadDetallePage;
  let fixture: ComponentFixture<NovedadDetallePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NovedadDetallePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NovedadDetallePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
