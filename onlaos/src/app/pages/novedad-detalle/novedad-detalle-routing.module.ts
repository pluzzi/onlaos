import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NovedadDetallePage } from './novedad-detalle.page';

const routes: Routes = [
  {
    path: '',
    component: NovedadDetallePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NovedadDetallePageRoutingModule {}
