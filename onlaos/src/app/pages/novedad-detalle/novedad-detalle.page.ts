import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NovedadesService } from 'src/app/services/novedades.service';

@Component({
  selector: 'app-novedad-detalle',
  templateUrl: './novedad-detalle.page.html',
  styleUrls: ['./novedad-detalle.page.scss'],
})
export class NovedadDetallePage implements OnInit {

  novedad: any

  constructor(
    private route: ActivatedRoute,
    private novService: NovedadesService
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.novService.getNovedad(params['id']).subscribe(result => {
        this.novedad = result[0]
      })
    })
  }

}
