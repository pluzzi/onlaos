import { Component, OnInit } from '@angular/core';
import { NavController, ToastController } from '@ionic/angular';
import { UsuariosService } from 'src/app/services/usuarios.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  nombre: string
  email: string
  password: string

  constructor(
    private navCtrl: NavController,
    private usuarios: UsuariosService,
    public toastController: ToastController
  ) { }

  ngOnInit() {
  }

  onEnter(){
    this.usuarios.register(this.nombre, this.email, this.password).subscribe(
      result =>{
        if(result['code']==0){
          this.presentToast(result['message'])
        }else{
          this.usuarios.setUsuario(result)
          this.navCtrl.navigateRoot(['/init']);
        }
      }
    )
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

  goToLogin(){
    this.navCtrl.navigateRoot(['/login']);
  }

}
