import { Component, OnInit } from '@angular/core';
import { Empresa } from 'src/app/interfaces/empresa';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';
import { EmpresasService } from 'src/app/services/empresas.service';
import { analyzeAndValidateNgModules } from '@angular/compiler';

@Component({
  selector: 'app-empresa',
  templateUrl: './empresa.page.html',
  styleUrls: ['./empresa.page.scss'],
})
export class EmpresaPage implements OnInit {

  id: number;
  empresa: Empresa
  
  constructor(
    private route: ActivatedRoute,
    private navCtrl: NavController,
    private empresasService: EmpresasService
  ) {
    this.route.params.subscribe(params => {
      this.id = params['id']
      this.empresasService.getEmpresa(this.id).subscribe(result =>{
        this.empresa = result[0]
      })
    });
  }

  ngOnInit() {
  }

  goto(){
    this.navCtrl.navigateRoot(['/menu/dialogo', { id: this.empresa.id }]);
  }

}
