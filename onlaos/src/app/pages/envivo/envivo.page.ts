import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ModalController } from '@ionic/angular';
import { VivoService } from 'src/app/services/vivo.service';
import { InfoPage } from '../info/info.page';

@Component({
  selector: 'app-envivo',
  templateUrl: './envivo.page.html',
  styleUrls: ['./envivo.page.scss'],
})
export class EnvivoPage implements OnInit {

  vivo: any
  trustedVideoUrl: any

  constructor(
    private vivoService: VivoService,
    private sanitizer: DomSanitizer,
    public modalController: ModalController
  ) { }

  ngOnInit() {
    this.vivoService.getLastVivo().subscribe(result =>{
      this.vivo = result[0]
      this.trustedVideoUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.vivo.url);
    })
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: InfoPage,
      cssClass: 'modal-class'
    });
    return await modal.present();
  }

  showInfo(){
    this.presentModal()
  }
}
