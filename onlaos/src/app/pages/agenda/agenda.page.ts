import { Component, OnInit } from '@angular/core';
import { LoadingController, NavController } from '@ionic/angular';
import { AgendasService } from 'src/app/services/agendas.service';

@Component({
  selector: 'app-agenda',
  templateUrl: './agenda.page.html',
  styleUrls: ['./agenda.page.scss'],
})
export class AgendaPage implements OnInit {

  agendas = new Array()
  page: number = 0
  loading: any

  constructor(
    private navCtrl: NavController,
    private agendaService: AgendasService,
    public loadingController: LoadingController
  ) { }

  ngOnInit() {
    this.presentLoading()
    this.addMoreAgendas(undefined)
  }

  verDetalle(agenda){
    this.navCtrl.navigateRoot(['/menu/agenda-detalle', { id: agenda.idAgenda }]);
  }

  loadData(event) {
    this.addMoreAgendas(event)
  }

  addMoreAgendas(event){
    this.agendaService.getAgendas(this.page).subscribe(
      result =>{
        if(this.agendas.length != 0){
          if(result.code == undefined){
            result.forEach(element => {
              this.agendas.push(element)
            });
          }
        }else{
          this.agendas = result
        }
        
        this.loading.dismiss()
        
        if(event!=undefined){
          event.target.complete()
        }
        
        this.page = this.page + 1
      }
    )
  }

  async presentLoading() {
    this.loading = await this.loadingController.create({
      message: 'Cargando datos...',
      duration: 0
    });
    await this.loading.present();
  }

}
