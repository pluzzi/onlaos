import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AgendaDetallePageRoutingModule } from './agenda-detalle-routing.module';

import { AgendaDetallePage } from './agenda-detalle.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AgendaDetallePageRoutingModule
  ],
  declarations: [AgendaDetallePage]
})
export class AgendaDetallePageModule {}
