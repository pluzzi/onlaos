import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AgendasService } from 'src/app/services/agendas.service';

@Component({
  selector: 'app-agenda-detalle',
  templateUrl: './agenda-detalle.page.html',
  styleUrls: ['./agenda-detalle.page.scss'],
})
export class AgendaDetallePage implements OnInit {

  agenda: any

  constructor(
    private route: ActivatedRoute,
    private agendaService: AgendasService
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.agendaService.getAgenda(params['id']).subscribe(result => {
        this.agenda = result[0]
      })
    })
  }

}
