import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AgendaDetallePage } from './agenda-detalle.page';

describe('AgendaDetallePage', () => {
  let component: AgendaDetallePage;
  let fixture: ComponentFixture<AgendaDetallePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgendaDetallePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AgendaDetallePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
