import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AgendaDetallePage } from './agenda-detalle.page';

const routes: Routes = [
  {
    path: '',
    component: AgendaDetallePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AgendaDetallePageRoutingModule {}
