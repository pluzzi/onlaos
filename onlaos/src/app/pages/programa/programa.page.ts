import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { Programa } from 'src/app/interfaces/programa';
import { ProgramasService } from 'src/app/services/programas.service';

@Component({
  selector: 'app-programa',
  templateUrl: './programa.page.html',
  styleUrls: ['./programa.page.scss'],
})
export class ProgramaPage implements OnInit {
  id: number
  programa: any
  trustedVideoUrl: any

  constructor(
    private route: ActivatedRoute,
    private proService: ProgramasService,
    private sanitizer: DomSanitizer
  ) {
    this.route.params.subscribe(params => {
      this.id = params['id']

      this.proService.getPrograma(this.id).subscribe(result =>{
        this.programa = result[0]
        this.trustedVideoUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.programa.url);
      })
    })
  }

  ngOnInit() {
  }

}
