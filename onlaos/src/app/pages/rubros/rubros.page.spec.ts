import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RubrosPage } from './rubros.page';

describe('RubrosPage', () => {
  let component: RubrosPage;
  let fixture: ComponentFixture<RubrosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RubrosPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RubrosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
