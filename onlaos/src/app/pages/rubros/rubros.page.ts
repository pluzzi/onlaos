import { Component, OnInit } from '@angular/core';
import { RubrosService } from 'src/app/services/rubros.service';

@Component({
  selector: 'app-rubros',
  templateUrl: './rubros.page.html',
  styleUrls: ['./rubros.page.scss'],
})
export class RubrosPage implements OnInit {

  items: any

  constructor(
    private rubroService: RubrosService
  ) { }

  ngOnInit() {
    this.rubroService.getRubros().subscribe(result =>{
      this.items = result
    })
  }

}
