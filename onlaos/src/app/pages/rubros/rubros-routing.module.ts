import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RubrosPage } from './rubros.page';

const routes: Routes = [
  {
    path: '',
    component: RubrosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RubrosPageRoutingModule {}
