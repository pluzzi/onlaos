import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RubrosPageRoutingModule } from './rubros-routing.module';

import { RubrosPage } from './rubros.page';

import { RubroItemComponent } from '../../components/rubro-item/rubro-item.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RubrosPageRoutingModule
  ],
  declarations: [RubrosPage, RubroItemComponent]
})
export class RubrosPageModule {}
