import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FeriaPageRoutingModule } from './feria-routing.module';

import { FeriaPage } from './feria.page';

import { EmpresaItemComponent } from '../../components/empresa-item/empresa-item.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FeriaPageRoutingModule
  ],
  declarations: [FeriaPage, EmpresaItemComponent]
})
export class FeriaPageModule {}
