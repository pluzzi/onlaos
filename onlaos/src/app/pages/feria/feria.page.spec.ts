import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FeriaPage } from './feria.page';

describe('FeriaPage', () => {
  let component: FeriaPage;
  let fixture: ComponentFixture<FeriaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeriaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FeriaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
