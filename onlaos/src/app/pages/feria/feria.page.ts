import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Rubro } from 'src/app/interfaces/rubro';
import { Empresa } from 'src/app/interfaces/empresa';
import { NumberFormatStyle } from '@angular/common';
import { EmpresasService } from 'src/app/services/empresas.service';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-feria',
  templateUrl: './feria.page.html',
  styleUrls: ['./feria.page.scss'],
})
export class FeriaPage implements OnInit {
  rubro: any;
  empresas = new Array()
  page: number = 0
  //loading: any

  constructor(
    private route: ActivatedRoute,
    private empresasService: EmpresasService,
    private loadingController: LoadingController
  ) {
    this.route.params.subscribe(params => {
      this.rubro = {
        idRubro: params['id'],
        nombre: params['rubro']
      };
    });
  }

  ngOnInit() {
    //this.presentLoading();
    this.addMoreEmpresas(undefined)
  }

  loadData(event) {
    this.addMoreEmpresas(event)
  }

  addMoreEmpresas(event){
    this.empresasService.getEmpresas(this.rubro.idRubro, this.page).subscribe(
      result =>{
        if(result.code == undefined){
          if(this.empresas.length != 0){
            result.forEach(element => {
              this.empresas.push(element)
            });
          }else{
            this.empresas = result
          }
        }

        /*
        if(this.loading!=undefined){
          this.loading.dismiss()
        }
        */      
        if(event!=undefined){
          event.target.complete()
        }
        
        this.page = this.page + 1
      }
    )
  }

  /*
  async presentLoading() {
    this.loading = await this.loadingController.create({
      message: 'Cargando datos...',
      duration: 0
    });
    await this.loading.present();
  }
  */
}
