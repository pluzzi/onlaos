import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FeriaPage } from './feria.page';

const routes: Routes = [
  {
    path: '',
    component: FeriaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FeriaPageRoutingModule {}
