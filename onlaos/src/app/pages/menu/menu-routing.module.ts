import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MenuPage } from './menu.page';

const routes: Routes = [
  {
    path: '',
    component: MenuPage,
    children: [
      {
        path: 'home',
        loadChildren: () => import('../home/home.module').then( m => m.HomePageModule)
      },
      {
        path: 'feria',
        loadChildren: () => import('../feria/feria.module').then( m => m.FeriaPageModule)
      },
      {
        path: 'programas',
        loadChildren: () => import('../programas/programas.module').then( m => m.ProgramasPageModule)
      },
      {
        path: 'agenda',
        loadChildren: () => import('../agenda/agenda.module').then( m => m.AgendaPageModule)
      },
      {
        path: 'novedades',
        loadChildren: () => import('../novedades/novedades.module').then( m => m.NovedadesPageModule)
      },
      {
        path: 'prodyserv',
        loadChildren: () => import('../prodyserv/prodyserv.module').then( m => m.ProdyservPageModule)
      },
      {
        path: 'envivo',
        loadChildren: () => import('../envivo/envivo.module').then( m => m.EnvivoPageModule)
      },
      {
        path: 'rubros',
        loadChildren: () => import('../rubros/rubros.module').then( m => m.RubrosPageModule)
      },
      {
        path: 'empresa',
        loadChildren: () => import('../empresa/empresa.module').then( m => m.EmpresaPageModule)
      },
      {
        path: 'dialogo',
        loadChildren: () => import('../dialogo/dialogo.module').then( m => m.DialogoPageModule)
      },
      {
        path: 'programa',
        loadChildren: () => import('../programa/programa.module').then( m => m.ProgramaPageModule)
      },
      {
        path: 'agenda-detalle',
        loadChildren: () => import('../agenda-detalle/agenda-detalle.module').then( m => m.AgendaDetallePageModule)
      },
      {
        path: 'novedad-detalle',
        loadChildren: () => import('../novedad-detalle/novedad-detalle.module').then( m => m.NovedadDetallePageModule)
      }

    ]
  },
  {
    path: 'menu',
    redirectTo: '/menu/feria'
  }
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MenuPageRoutingModule {}
