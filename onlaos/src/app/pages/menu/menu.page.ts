import { Component, OnInit } from '@angular/core';
import { Router, RouterEvent } from '@angular/router';
import { NavController } from '@ionic/angular';
import { UsuariosService } from 'src/app/services/usuarios.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {

  pages = [
    {
      title: 'Inicio',
      url: '/menu/home'
    },
    {
      title: 'Gestión Empresas',
      url: '/menu/rubros'
    },
    {
      title: 'Programas',
      url: '/menu/programas'
    },
    {
      title: 'Agenda',
      url: '/menu/agenda'
    },
    {
      title: 'Novedades',
      url: '/menu/novedades'
    },
    {
      title: 'Productos y servicios',
      url: '/menu/prodyserv'
    },
    {
      title: 'Emisiones en vivo',
      url: '/menu/envivo'
    }
  ];

  selectedPath = '';
  verMenu = false;

  constructor(
    private router: Router,
    private userSrv: UsuariosService
  ) { 
    this.router.events.subscribe((event: RouterEvent) => {
      this.selectedPath = event.url;
      if(this.selectedPath==="/menu/home"){
        this.verMenu = true
      }else{
        this.verMenu = false
      }
    });
  }

  ngOnInit() {
  }

  logout(){
    this.userSrv.logout();
  }

}
