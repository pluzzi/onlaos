import { Component, OnInit } from '@angular/core';
import { Programa } from 'src/app/interfaces/programa';
import { LoadingController, NavController } from '@ionic/angular';
import { ProgramasService } from 'src/app/services/programas.service';

@Component({
  selector: 'app-programas',
  templateUrl: './programas.page.html',
  styleUrls: ['./programas.page.scss'],
})
export class ProgramasPage implements OnInit {

  programas = new Array()
  page: number = 0
  loading: any

  constructor(
    private navCtrl: NavController,
    private proServices: ProgramasService,
    public loadingController: LoadingController
  ) {
  }

  ngOnInit() {
    //this.presentLoading()
    this.addMoreProgramas(undefined)
  }

  isColor1(i): boolean{
    let r = i % 12
    return r>=0 && r<3
  }

  isColor2(i): boolean{
    let r = i % 12
    return r>=3 && r<9
  }

  isColor3(i): boolean{
    let r = i % 12
    return r>=9 && r<12
  }

  goto(programa){
    this.navCtrl.navigateRoot(['/menu/programa', { id: programa.idPrograma }]);
  }

  loadData(event) {
    this.addMoreProgramas(event)
  }

  addMoreProgramas(event){
    this.proServices.getProgramas(this.page).subscribe(
      result =>{
        if(this.programas.length != 0){
          result.forEach(element => {
            this.programas.push(element)
          });
        }else{
          this.programas = result
        }
                
        if(this.loading!=undefined){
          this.loading.dismiss()
        }

        if(event!=undefined){
          event.target.complete()
        }
        
        this.page = this.page + 1
      }
    )
  }

  async presentLoading() {
    this.loading = await this.loadingController.create({
      message: 'Cargando datos...',
      duration: 0
    });
    await this.loading.present();
  }
}
