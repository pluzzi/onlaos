import { Component, OnInit, Input } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Empresa } from 'src/app/interfaces/empresa';

@Component({
  selector: 'app-empresa-item',
  templateUrl: './empresa-item.component.html',
  styleUrls: ['./empresa-item.component.scss'],
})
export class EmpresaItemComponent implements OnInit {

  @Input() item: Empresa;

  constructor(
    private navCtrl: NavController
  ) { }

  ngOnInit() {}

  goto(){
    this.navCtrl.navigateRoot(['/menu/empresa', { id: this.item.id }]);
  }
}
