import { Component, OnInit, Input  } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Rubro } from 'src/app/interfaces/rubro';

@Component({
  selector: 'app-rubro-item',
  templateUrl: './rubro-item.component.html',
  styleUrls: ['./rubro-item.component.scss'],
})
export class RubroItemComponent implements OnInit {

  @Input() item: Rubro;

  constructor(
    private navCtrl: NavController
  ) { }

  ngOnInit() {}

  goto(){
    this.navCtrl.navigateRoot(['/menu/feria', { id: this.item.idRubro, rubro: this.item.nombre }]);
  }

}
