import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RubroItemComponent } from './rubro-item.component';

describe('RubroItemComponent', () => {
  let component: RubroItemComponent;
  let fixture: ComponentFixture<RubroItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RubroItemComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RubroItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
