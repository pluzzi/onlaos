<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Onlaos Admin</title>

    <!-- Vendor CSS -->
    <link href=<?php echo "'/admin_new/vendors/bower_components/fullcalendar/dist/fullcalendar.min.css'"; ?> rel="stylesheet">
    <link href=<?php echo "'/admin_new/vendors/bower_components/animate.css/animate.min.css'"; ?> rel="stylesheet">
    <link href=<?php echo "'/admin_new/vendors/bower_components/sweetalert2/dist/sweetalert2.min.css'"; ?> rel="stylesheet">
    <link href=<?php echo "'/admin_new/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css'"; ?> rel="stylesheet">
    <link href=<?php echo "'/admin_new/vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css'"; ?> rel="stylesheet">
    <link href=<?php echo "'/admin_new/vendors/bower_components/datatables.net-dt/css/jquery.dataTables.min.css'"; ?> rel="stylesheet">
    <link href=<?php echo "'/admin_new/vendors/summernote/dist/summernote.css'"; ?> rel="stylesheet">


    <!-- CSS -->
    <link href=<?php echo "'/admin_new/css/app_1.min.css'"; ?> rel="stylesheet">
    <link href=<?php echo "'/admin_new/css/app_2.min.css'"; ?> rel="stylesheet">

</head>

<!-- Javascript Libraries -->
<script src=<?php echo "'/admin_new/vendors/bower_components/jquery/dist/jquery.min.js'"; ?>></script>
<script src=<?php echo "'/admin_new/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js'"; ?>></script>

<script src=<?php echo "'/admin_new/vendors/bower_components/flot/jquery.flot.js'"; ?>></script>
<script src=<?php echo "'/admin_new/vendors/bower_components/flot/jquery.flot.resize.js'"; ?>></script>
<script src=<?php echo "'/admin_new/vendors/bower_components/flot.curvedlines/curvedLines.js'"; ?>></script>
<script src=<?php echo "'/admin_new/vendors/sparklines/jquery.sparkline.min.js'"; ?>></script>
<script src=<?php echo "'/admin_new/vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js'"; ?>></script>

<script src=<?php echo "'/admin_new/vendors/bower_components/moment/min/moment.min.js'"; ?>></script>
<script src=<?php echo "'/admin_new/vendors/bower_components/fullcalendar/dist/fullcalendar.min.js'"; ?>></script>
<script src=<?php echo "'/admin_new/vendors/bower_components/simpleWeather/jquery.simpleWeather.min.js'"; ?>></script>
<script src=<?php echo "'/admin_new/vendors/bower_components/Waves/dist/waves.min.js'"; ?>></script>
<script src=<?php echo "'/admin_new/vendors/bootstrap-growl/bootstrap-growl.min.js'"; ?>></script>
<script src=<?php echo "'/admin_new/vendors/bower_components/sweetalert2/dist/sweetalert2.min.js'"; ?>></script>
<script src=<?php echo "'/admin_new/vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js'"; ?>></script>
<script src=<?php echo "'/admin_new/vendors/bower_components/datatables.net/js/jquery.dataTables.min.js'"; ?>></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#data-table-basic').DataTable();
    } );
</script>

<script src=<?php echo "'/admin_new/js/app.min.js'"; ?>></script>

<script src=<?php echo "'/admin_new/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js'"; ?> ></script>
<script src=<?php echo "'/admin_new/vendors/summernote/dist/summernote-updated.min.js'"; ?> ></script>

<script src=<?php echo "'/admin_new/vendors/fileinput/fileinput.min.js'"; ?> ></script>


