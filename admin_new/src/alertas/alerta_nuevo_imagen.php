<?php
	if (isset($_GET['error'])){
		if($_GET['error']=="0"){
			echo '
			    <div class="alert alert-success alert-dismissible" role="alert">
			        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
			                aria-hidden="true">&times;</span></button>
			        La galería de imágen se guardo con exito.
			    </div>';
		}else{
			echo '
			    <div class="alert alert-danger alert-dismissible" role="alert">
			        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
			                aria-hidden="true">&times;</span></button>
			        La galería de imágen no se guardo, verifique los datos.
			    </div>';
		}
	}

?>