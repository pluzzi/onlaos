<!DOCTYPE html>
<?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/headers/header.php"); ?>
    <body>
    <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/login/islogin.php"); ?>
    <?php $_SESSION['idPagina'] = 1; ?>

        <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/menu/menu.php"); ?>

        <section id="main">
            <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/sidebar/sidebar.php"); ?>

            <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/chat/chat.php"); ?>

            <section id="content">
                <?php
                    include($_SERVER['DOCUMENT_ROOT']."/admin_new/config/conexionSQL.php");
                    $id  = $_GET['id'];
                    $query = "select idusuario, usuario, password, nombre, idmodulo, publicado from usuarios where idusuario = ".$id;

                    $result = mysqli_query($link, $query) or die (mysql_error());

                    $row = mysqli_fetch_array($result)

                ?>
                <div class="container">
                    <div class="block-header">
                        <h2>Está en el panel de administración de Onlaos.com</h2>
                    </div>

                    <div class="card">
                        <div class="card-header">
                            <h2>Nuevo Usuario del Sistema
                                <small>Complete los datos del nuevo usuario.
                                </small>
                            </h2>
                        </div>

                        <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/alertas/alerta_nuevo_usuario.php"); ?>

                        <div class="card-body card-padding">
                            <form role="form" action=<?php echo "/admin_new/src/usuarios/guardar_usuario.php?id=".$_GET['id']; ?> method="post">
                                <div class="form-group fg-line">
                                    <label for="inputNombre">Nombre</label>
                                    <input type="text" class="form-control input-sm" id="inputNombre" name="inputNombre"
                                           placeholder="Ingresar nombre" value=<?php echo '"'.$row['nombre'].'"' ?>>
                                </div>
                                <div class="form-group fg-line">
                                    <label for="inputUsuario">Usuario</label>
                                    <input type="text" class="form-control input-sm" id="inputUsuario" name="inputUsuario"
                                           placeholder="Ingresar Usuario" value=<?php echo '"'.$row['usuario'].'"' ?>>
                                </div>
                                <div class="form-group fg-line">
                                    <label for="inputPassword">Pasword</label>
                                    <input type="text" class="form-control input-sm" id="inputPassword" name="inputPassword"
                                           placeholder="Ingresar password" value=<?php echo '"'.$row['password'].'"' ?>>
                                </div>
                                <div class="form-group fg-line">
                                    <label for="inputRePassword">Confirmar password</label>
                                    <input type="text" class="form-control input-sm" id="inputRePassword" name="inputRePassword"
                                           placeholder="Ingresar password" value=<?php echo '"'.$row['password'].'"' ?>>
                                </div>
                                <div class="form-group fg-line">
                                <label>Permisos</label><br>
                                    <div class="checkbox m-b-15">
                                        <label>
                                            <input type="checkbox" name="chkUsuario"
                                            <?php $pos = strrpos($row['idmodulo'], "1"); if($pos===false){ echo ''; }else{ echo 'checked="true"'; }  ?>>
                                            <i class="input-helper"></i>
                                            Usuarios
                                        </label>
                                    </div>
                                    <div class="checkbox m-b-15">
                                        <label>
                                            <input type="checkbox" name="chkAgenda"
                                            <?php $pos = strrpos($row['idmodulo'], "2"); if($pos===false){ echo ''; }else{ echo 'checked="true"'; }  ?>>
                                            <i class="input-helper"></i>
                                            Agenda
                                        </label>
                                    </div>
                                    <div class="checkbox m-b-15">
                                        <label>
                                            <input type="checkbox" name="chkCategorias"
                                            <?php $pos = strrpos($row['idmodulo'], "3"); if($pos===false){ echo ''; }else{ echo 'checked="true"'; }  ?>>
                                            <i class="input-helper"></i>
                                            Categorías
                                        </label>
                                    </div>
                                    <div class="checkbox m-b-15">
                                        <label>
                                            <input type="checkbox" name="chkCategoriaVideos"
                                            <?php $pos = strrpos($row['idmodulo'], "4"); if($pos===false){ echo ''; }else{ echo 'checked="true"'; }  ?>>
                                            <i class="input-helper"></i>
                                            Categorías de videos
                                        </label>
                                    </div>
                                    <div class="checkbox m-b-15">
                                        <label>
                                            <input type="checkbox" name="chkGaleria"
                                            <?php $pos = strrpos($row['idmodulo'], "5"); if($pos===false){ echo ''; }else{ echo 'checked="true"'; }  ?>>
                                            <i class="input-helper"></i>
                                            Galería de imágenes
                                        </label>
                                    </div>
                                    <div class="checkbox m-b-15">
                                        <label>
                                            <input type="checkbox" name="chkNovedades"
                                            <?php $pos = strrpos($row['idmodulo'], "6"); if($pos===false){ echo ''; }else{ echo 'checked="true"'; }  ?>>
                                            <i class="input-helper"></i>
                                            Novedades
                                        </label>
                                    </div>
                                    <div class="checkbox m-b-15">
                                        <label>
                                            <input type="checkbox" name="chkProgramas"
                                            <?php $pos = strrpos($row['idmodulo'], "7"); if($pos===false){ echo ''; }else{ echo 'checked="true"'; }  ?>>
                                            <i class="input-helper"></i>
                                            Programas
                                        </label>
                                    </div>
                                    <div class="checkbox m-b-15">
                                        <label>
                                            <input type="checkbox" name="chkEmpresas"
                                            <?php $pos = strrpos($row['idmodulo'], "9"); if($pos===false){ echo ''; }else{ echo 'checked="true"'; }  ?>>
                                            <i class="input-helper"></i>
                                            Servicios y Empresas
                                        </label>
                                    </div>
                                    
                                </div>
                                
                                
                                <button type="submit" class="btn btn-primary btn-sm m-t-10">Guardar</button>
                            </form>
                        </div>
                    </div>

                </div>
            </section>


        </section>

        <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/footers/footer.php"); ?>

        <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/loader/loader.php"); ?>

    </body>
  </html>
