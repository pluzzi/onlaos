<!DOCTYPE html>
<?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/headers/header.php"); ?>
    <body>
    <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/login/islogin.php"); ?>
    <?php $_SESSION['idPagina'] = 1; ?>

        <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/menu/menu.php"); ?>

        <section id="main">
            <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/sidebar/sidebar.php"); ?>

            <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/chat/chat.php"); ?>

            <section id="content">
                <div class="container">
                    <div class="block-header">
                        <h2>Está en el panel de administración de Onlaos.com</h2>
                    </div>

                    <div class="card">
                        <div class="card-header">
                            <h2>Nuevo Usuario del Sistema
                                <small>Complete los datos del nuevo usuario.
                                </small>
                            </h2>
                        </div>

                        <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/alertas/alerta_nuevo_usuario.php"); ?>

                        <div class="card-body card-padding">
                            <form role="form" action="/admin_new/src/usuarios/guardar_usuario.php?id=0" method="post">
                                <div class="form-group fg-line">
                                    <label for="inputNombre">Nombre</label>
                                    <input type="text" class="form-control input-sm" id="inputNombre" name="inputNombre"
                                           placeholder="Ingresar nombre">
                                </div>
                                <div class="form-group fg-line">
                                    <label for="inputUsuario">Usuario</label>
                                    <input type="text" class="form-control input-sm" id="inputUsuario" name="inputUsuario"
                                           placeholder="Ingresar Usuario">
                                </div>
                                <div class="form-group fg-line">
                                    <label for="inputPassword">Pasword</label>
                                    <input type="text" class="form-control input-sm" id="inputPassword" name="inputPassword"
                                           placeholder="Ingresar password">
                                </div>
                                <div class="form-group fg-line">
                                    <label for="inputRePassword">Confirmar password</label>
                                    <input type="text" class="form-control input-sm" id="inputRePassword" name="inputRePassword"
                                           placeholder="Ingresar password">
                                </div>
                                <div class="form-group fg-line">
                                <label>Permisos</label><br>
                                    <div class="checkbox m-b-15">
                                        <label>
                                            <input type="checkbox" value="" name="chkUsuario">
                                            <i class="input-helper"></i>
                                            Usuarios
                                        </label>
                                    </div>
                                    <div class="checkbox m-b-15">
                                        <label>
                                            <input type="checkbox" value="" name="chkAgenda">
                                            <i class="input-helper"></i>
                                            Agenda
                                        </label>
                                    </div>
                                    <div class="checkbox m-b-15">
                                        <label>
                                            <input type="checkbox" value="" name="chkCategorias">
                                            <i class="input-helper"></i>
                                            Categorías
                                        </label>
                                    </div>
                                    <div class="checkbox m-b-15">
                                        <label>
                                            <input type="checkbox" value="" name="chkCategoriaVideos">
                                            <i class="input-helper"></i>
                                            Categorías de videos
                                        </label>
                                    </div>
                                    <div class="checkbox m-b-15">
                                        <label>
                                            <input type="checkbox" value="" name="chkGaleria">
                                            <i class="input-helper"></i>
                                            Galería de imágenes
                                        </label>
                                    </div>
                                    <div class="checkbox m-b-15">
                                        <label>
                                            <input type="checkbox" value="" name="chkNovedades">
                                            <i class="input-helper"></i>
                                            Novedades
                                        </label>
                                    </div>
                                    <div class="checkbox m-b-15">
                                        <label>
                                            <input type="checkbox" value="" name="chkProgramas">
                                            <i class="input-helper"></i>
                                            Programas
                                        </label>
                                    </div>
                                    <div class="checkbox m-b-15">
                                        <label>
                                            <input type="checkbox" value="" name="chkEmpresas">
                                            <i class="input-helper"></i>
                                            Servicios y Empresas
                                        </label>
                                    </div>
                                    
                                </div>
                                
                                
                                <button type="submit" class="btn btn-primary btn-sm m-t-10">Guardar</button>
                            </form>
                        </div>
                    </div>

                </div>
            </section>


        </section>

        <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/footers/footer.php"); ?>

        <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/loader/loader.php"); ?>

    </body>
  </html>
