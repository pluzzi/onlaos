<!DOCTYPE html>
<?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/headers/header.php"); ?>
    <body>
    <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/login/islogin.php"); ?>
    <?php $_SESSION['idPagina'] = 1; ?>

        <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/menu/menu.php"); ?>

        <section id="main">
            <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/sidebar/sidebar.php"); ?>

            <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/chat/chat.php"); ?>

            <section id="content">
                <div class="container">
                    <div class="block-header">
                        <h2>Está en el panel de administración de Onlaos.com</h2>
                    </div>

                    <div class="card">
                        <div class="card-header">
                            <h2>Lista de usuarios
                                <small>En esta sección se podrán administrar los usuarios del sistema.
                                </small>

                            </h2>
                            <h1><a href="nuevo_usuario.php"><i class="zmdi zmdi-account-add"></i></a></h1>
                        </div>

                        <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/alertas/alerta_nuevo_usuario.php"); ?>

                        <div class="table-responsive">
                            <table id="data-table-basic" class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Usuario</th>
                                    <th>Nombre</th>
                                    <th>Permisos</th>
                                    <th>Acción</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>Id</th>
                                    <th>Usuario</th>
                                    <th>Nombre</th>
                                    <th>Permisos</th>
                                    <th>Acción</th>
                                </tr>
                                </tfoot>
                                <tbody>
                                <?php
                                    include($_SERVER['DOCUMENT_ROOT']."/admin_new/config/conexionSQL.php");

                                    $query = "select idusuario, usuario, password, nombre, idmodulo, publicado from usuarios";
                                    $result = mysqli_query($link, $query) or die (mysql_error());

                                    while($row = mysqli_fetch_array($result)) {
                                        echo '<tr>
                                                <td>'.$row['idusuario'].'</td>
                                                <td>'.$row['usuario'].'</td>
                                                <td>'.$row['nombre'].'</td>
                                                <td>'.$row['idmodulo'].'</td>
                                                <td>
                                                   <h4>
                                                        <a href="editar_usuario.php?id='.$row['idusuario'].'"><i class="zmdi zmdi-edit"></i></a>
                                                        <a onclick="confirmar('.$row['idusuario'].')" ><i class="zmdi zmdi-delete"></i></a>
                                                   </h4>
                                                </td>
                                            </tr>';

                                    }


                                ?>
                               
                            </table>
                        </div>
                    </div>

                </div>
            </section>


        </section>

        <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/footers/footer.php"); ?>

        <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/loader/loader.php"); ?>

    </body>
    <script type="text/javascript">
    //Parameter
    function confirmar(id){
        swal({   
            title: "Está seguro?",   
            text: "No podrá recuperar el usuario!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonText: "Si, borrarlo!",
            cancelButtonText: "No, cancelar!",   
        }).then(function(isConfirm){
            
            if(isConfirm){
                
                swal("Borrado!", "El usuario ha sido borado.", "success").then(function(){ window.location = "/admin_new/src/usuarios/eliminar_usuario.php?id="+id; });

            }

        });

    };

</script>

  </html>
