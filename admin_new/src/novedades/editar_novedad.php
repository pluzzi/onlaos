<!DOCTYPE html>
<?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/headers/header.php"); ?>
    <body>
    <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/login/islogin.php"); ?>
    <?php $_SESSION['idPagina'] = 6; ?>

        <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/menu/menu.php"); ?>

        <section id="main">
            <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/sidebar/sidebar.php"); ?>

            <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/chat/chat.php"); ?>

            <section id="content">
                <?php
                    include($_SERVER['DOCUMENT_ROOT']."/admin_new/config/conexionSQL.php");
                    $id  = $_GET['id'];
                    $query = "select idnovedad, titulo, copete, cuerpo, foto, DATE_FORMAT(fechapublicacion,'%d/%m/%Y') as fechapublicacion, publicado from novedades where idnovedad = ".$id;

                    $result = mysqli_query($link, $query) or die (mysql_error());

                    $row = mysqli_fetch_array($result)

                ?>
                <div class="container">
                    <div class="block-header">
                        <h2>Está en el panel de administración de Onlaos.com</h2>
                    </div>

                    <div class="card">
                        <div class="card-header">
                            <h2>Editar Novedad del Sistema
                                
                            </h2>
                        </div>

                        <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/alertas/alerta_nuevo_novedad.php"); ?>

                        <div class="card-body card-padding">
                            <form role="form" action=<?php echo "/admin_new/src/novedades/guardar_novedad.php?id=".$_GET['id']; ?> method="post" enctype="multipart/form-data">
                                <div class="form-group fg-line">
                                    <label for="inputTitulo">Título</label>
                                    <input type="text" class="form-control input-sm" id="inputTitulo" name="inputTitulo"
                                           placeholder="Ingresar Título" value=<?php echo '"'.$row['titulo'].'"' ?>>
                                </div>
                                <div class="form-group fg-line">
                                    <label for="inputCopete">Copete</label>
                                    <input type="text" class="form-control input-sm" id="inputCopete" name="inputCopete"
                                           placeholder="Ingresar Copete" value=<?php echo '"'.$row['copete'].'"' ?>>
                                </div>
                                

                                <div class="form-group fg-line">
                                    <label for="inputCuerpo">Cuerpo</label>
                                    <textarea class="html-editor" placeholder="Ingrese el cuerpo..." id="inputCuerpo" name="inputCuerpo">
                                        <?php echo $row['cuerpo']; ?>
                                    </textarea>
                                </div>

                                <div class="form-group fg-line">
                                    <label for="inputFoto">Foto</label><br>
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput">
                                            <?php echo '<img src="data:image/jpeg;base64,'.base64_encode( $row['foto'] ).'" />'; ?>
                                        </div>
                                        <div>
                                            <span class="btn btn-primary btn-file m-r-10">
                                                <span class="fileinput-new">Seleccionar imágen</span>
                                                <input type="file" id="inputFoto" name="inputFoto"  >
                                            </span>
                                            <a href="#" class="btn btn-danger fileinput-exists"
                                               data-dismiss="fileinput">Borrar</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="input-group form-line">
                                    <label for="inputFecha">Fecha</label>
                                    
                                    <input type="text" class="form-control date-picker" placeholder="Click aqui..." id="inputFecha" name="inputFecha" value=<?php echo $row['fechapublicacion']; ?>>
                                </div>

                                
                                <button type="submit" class="btn btn-primary btn-sm m-t-10">Guardar</button>
                            </form>
                        </div>
                    </div>

                </div>
            </section>


        </section>

        <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/footers/footer.php"); ?>

        <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/loader/loader.php"); ?>

    </body>
  </html>
