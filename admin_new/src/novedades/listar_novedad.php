<!DOCTYPE html>
<?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/headers/header.php"); ?>
    <body>
    <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/login/islogin.php"); ?>
    <?php $_SESSION['idPagina'] = 6; ?>

        <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/menu/menu.php"); ?>

        <section id="main">
            <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/sidebar/sidebar.php"); ?>

            <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/chat/chat.php"); ?>

            <section id="content">
                <div class="container">
                    <div class="block-header">
                        <h2>Está en el panel de administración de Onlaos.com</h2>
                    </div>

                    <div class="card">
                        <div class="card-header">
                            <h2>Lista de Novedades
                                <small>En esta sección se podrán administrar las novedades del sistema.
                                </small>

                            </h2>
                            <h1><a href="nuevo_novedad.php"><i class="zmdi zmdi-file-plus"></i></a></h1>
                        </div>

                        <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/alertas/alerta_nuevo_novedad.php"); ?>

                        <div class="table-responsive">
                            <table id="data-table-basic" class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Titulo</th>
                                    <th>Copete</th>
                                    <th>Fecha</th>
                                    <th>Acción</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>Id</th>
                                    <th>Titulo</th>
                                    <th>Copete</th>
                                    <th>Fecha</th>
                                    <th>Acción</th>
                                </tr>
                                </tfoot>
                                <tbody>
                                <?php
                                    include($_SERVER['DOCUMENT_ROOT']."/admin_new/config/conexionSQL.php");

                                    $query = "select idnovedad, titulo, copete, cuerpo, fechapublicacion, publicado from novedades";
                                    $result = mysqli_query($link, $query) or die (mysql_error());

                                    while($row = mysqli_fetch_array($result)) {
                                        echo '<tr>
                                                <td>'.$row['idnovedad'].'</td>
                                                <td>'.$row['titulo'].'</td>
                                                <td>'.$row['copete'].'</td>
                                                <td>'.$row['fechapublicacion'].'</td>
                                                <td>
                                                   <h4>
                                                        <a href="editar_novedad.php?id='.$row['idnovedad'].'"><i class="zmdi zmdi-edit"></i></a>
                                                        <a onclick="confirmar('.$row['idnovedad'].')" ><i class="zmdi zmdi-delete"></i></a>
                                                   </h4>
                                                </td>
                                            </tr>';

                                    }


                                ?>
                               
                            </table>
                        </div>
                    </div>

                </div>
            </section>


        </section>

        <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/footers/footer.php"); ?>

        <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/loader/loader.php"); ?>

    </body>
    <script type="text/javascript">
    //Parameter
    function confirmar(id){
        swal({   
            title: "Está seguro?",   
            text: "No podrá recuperar la novedad!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonText: "Si, borrarlo!",
            cancelButtonText: "No, cancelar!",   
        }).then(function(isConfirm){
            
            if(isConfirm){
                
                swal("Borrado!", "La novedad ha sido borada.", "success").then(function(){ window.location = "/admin_new/src/novedades/eliminar_novedad.php?id="+id; });

            }

        });

    };

</script>

  </html>
