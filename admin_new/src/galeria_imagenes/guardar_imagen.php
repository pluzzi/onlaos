<?php

// importo datos de conexion
include($_SERVER['DOCUMENT_ROOT']."/admin_new/config/conexionSQL.php");

// obtengo valores ingresados
$nombre 					= $_POST['inputNombre'];
$categoria 					= $_POST['inputCategoria'];

if(isset($_POST['inputFecha'])){
	$fecha 						= $_POST['inputFecha'];
	$aux = split("/", $fecha);
	$fecha = $aux[2]."-".$aux[1]."-".$aux[0];
}

// obtengo id (0: nuevo)
$id 						= $_GET['id'];

// valido campos
if($nombre != "" and $categoria != ""){
	$valido = true;
}else {
	$valido = false;
}

// valido subida de archivo
if (is_uploaded_file($_FILES["inputFoto"]["tmp_name"]))
{
    # verificamos el formato de la imagen
    if ($_FILES["inputFoto"]["type"]=="image/jpeg" || $_FILES["inputFoto"]["type"]=="image/pjpeg" || $_FILES["inputFoto"]["type"]=="image/gif" || $_FILES["inputFoto"]["type"]=="image/bmp" || $_FILES["inputFoto"]["type"]=="image/png")
    {
    	$conFoto = true;
    	$foto = $link->real_escape_string(file_get_contents($_FILES["inputFoto"]["tmp_name"]));

    }else{
    	$conFoto = false;
    }
}else{
	$conFoto = false;
}

// condiciones
if($id=='0' and $conFoto == false){
	$valido = false;
}

if ($valido){
	if($id=='0'){
		$sql = "INSERT INTO galerias (nombre, idcategoria, fotoseleccionada, fechacreacion)
		VALUES ('".$nombre."', '".$categoria."','".$foto."','".$fecha."')";
	}else{
		if($conFoto){
			
			$sql = "UPDATE galerias SET nombre='".$nombre."', idcategoria='".$categoria."', fotoseleccionada= '".$foto."'
			WHERE idgaleria =".$id;
			
		}else{
			
			$sql = "UPDATE galerias SET nombre='".$nombre."', idcategoria='".$categoria."'
			WHERE idgaleria =".$id;
			
		}
	}

	if ($link->query($sql) === TRUE) {
		header("Location: /admin_new/src/galeria_imagenes/listar_imagen.php?error=0");
	} else {
		if($id=="0"){
			header("Location: /admin_new/src/galeria_imagenes/nuevo_imagen.php?error=1");
		}else{
			header("Location: /admin_new/src/galeria_imagenes/listar_imagen.php?error=1");
		}
	}

	$link->close();
}else{
	if($id=="0"){
		header("Location: /admin_new/src/galeria_imagenes/nuevo_imagen.php?error=1");
	}else{
		header("Location: /admin_new/src/galeria_imagenes/listar_imagen.php?error=1");
	}
}



?>