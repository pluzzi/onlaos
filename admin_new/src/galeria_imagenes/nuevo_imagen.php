<!DOCTYPE html>
<?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/headers/header.php"); ?>
    <body>
    <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/login/islogin.php"); ?>
    <?php $_SESSION['idPagina'] = 5; ?>

        <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/menu/menu.php"); ?>

        <section id="main">
            <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/sidebar/sidebar.php"); ?>

            <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/chat/chat.php"); ?>

            <section id="content">
                <div class="container">
                    <div class="block-header">
                        <h2>Está en el panel de administración de Onlaos.com</h2>
                    </div>

                    <div class="card">
                        <div class="card-header">
                            <h2>Nueva Galería de Imágen del Sistema
                                <small>Complete los datos de la nueva Galería de Imágen.
                                </small>
                            </h2>
                        </div>

                        <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/alertas/alerta_nuevo_imagen.php"); ?>

                        <div class="card-body card-padding">
                            <form role="form" action="/admin_new/src/galeria_imagenes/guardar_imagen.php?id=0" method="post" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label for="inputNombre">Nombre</label>
                                    <input type="text" class="form-control input-sm" id="inputNombre" name="inputNombre"
                                           placeholder="Ingresar Nombre">
                                </div>

                                <div class="form-group">
                                    <div class="fg-line">
                                        <div class="select">
                                            <select class="form-control" id="inputCategoria" name="inputCategoria">
                                                <option>Seleccionar...</option>
                                                <?php
                                                    include($_SERVER['DOCUMENT_ROOT']."/admin_new/config/conexionSQL.php");

                                                    $query = "select idcategoria, titulo from categorias";

                                                    $result = mysqli_query($link, $query) or die (mysql_error());

                                                    while($row = mysqli_fetch_array($result)) {
                                                        echo '<option value="'.$row['idcategoria'].'">'.$row['titulo'].'</option>';
                                                    }

                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="input-group form-line">
                                    <label for="inputFecha">Fecha</label>
                                    
                                    <input type="text" class="form-control date-picker" placeholder="Click aqui..." id="inputFecha" name="inputFecha">
                                </div>

                                <br>

                                <div class="form-group fg-line">
                                    <label for="inputFoto">Foto</label><br>
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput"></div>
                                        <div>
                                            <span class="btn btn-primary btn-file m-r-10">
                                                <span class="fileinput-new">Seleccionar imágen</span>
                                                <input type="file" id="inputFoto" name="inputFoto"  >
                                            </span>
                                            <a href="#" class="btn btn-danger fileinput-exists"
                                               data-dismiss="fileinput">Borrar</a>
                                        </div>
                                    </div>
                                </div>
                                
                                <button type="submit" class="btn btn-primary btn-sm m-t-10">Guardar</button>
                            </form>
                        </div>
                    </div>

                </div>
            </section>


        </section>

        <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/footers/footer.php"); ?>

        <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/loader/loader.php"); ?>

    </body>
  </html>
