<!DOCTYPE html>
<?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/headers/header.php"); ?>
    <body>
    <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/login/islogin.php"); ?>
    <?php $_SESSION['idPagina'] = 5; ?>

        <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/menu/menu.php"); ?>

        <section id="main">
            <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/sidebar/sidebar.php"); ?>

            <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/chat/chat.php"); ?>

            <section id="content">
                <div class="container">
                    <div class="block-header">
                        <h2>Está en el panel de administración de Onlaos.com</h2>
                    </div>

                    <div class="card">
                        <div class="card-header">
                            <h2>Lista de Galerías de Imágenes
                                <small>En esta sección se podrán administrar la galería de imágenes del sistema.
                                </small>

                            </h2>
                            <h1><a href="nuevo_imagen.php"><i class="zmdi zmdi-file-plus"></i></a></h1>
                        </div>

                        <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/alertas/alerta_nuevo_imagen.php"); ?>

                        <div class="table-responsive">
                            <table id="data-table-basic" class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Foto</th>
                                    <th>Nombre</th>
                                    <th>Categoria</th>
                                    <th>Acción</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>Id</th>
                                    <th>Foto</th>
                                    <th>Nombre</th>
                                    <th>Categoria</th>
                                    <th>Acción</th>
                                </tr>
                                </tfoot>
                                <tbody>
                                <?php
                                    include($_SERVER['DOCUMENT_ROOT']."/admin_new/config/conexionSQL.php");

                                    $query = "select idgaleria, nombre, c.titulo as categoria
                                            from galerias g 
                                            left join categorias c on (g.idcategoria = c.idcategoria)
                                            order by c.idcategoria";

                                    $result = mysqli_query($link, $query) or die (mysql_error());

                                    while($row = mysqli_fetch_array($result)) {
                                        echo '<tr>
                                                <td>'.$row['idgaleria'].'</td>
                                                <td></td>
                                                <td>'.$row['nombre'].'</td>
                                                <td>'.$row['categoria'].'</td>
                                                <td>
                                                   <h4>
                                                        <a href="editar_imagen.php?id='.$row['idgaleria'].'"><i class="zmdi zmdi-edit"></i></a>
                                                        <a onclick="confirmar('.$row['idgaleria'].')" ><i class="zmdi zmdi-delete"></i></a>
                                                   </h4>
                                                </td>
                                            </tr>';

                                    }


                                ?>
                               
                            </table>
                        </div>
                    </div>

                </div>
            </section>


        </section>

        <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/footers/footer.php"); ?>

        <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/loader/loader.php"); ?>

    </body>
    <script type="text/javascript">
    //Parameter
    function confirmar(id){
        swal({   
            title: "Está seguro?",   
            text: "No podrá recuperar la imágen!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonText: "Si, borrarlo!",
            cancelButtonText: "No, cancelar!",   
        }).then(function(isConfirm){
            
            if(isConfirm){
                
                swal("Borrado!", "La imágen ha sido borada.", "success").then(function(){ window.location = "/admin_new/src/galeria_imagenes/eliminar_imagen.php?id="+id; });

            }

        });

    };

</script>

  </html>
