<?php

// importo datos de conexion
include($_SERVER['DOCUMENT_ROOT']."/admin_new/config/conexionSQL.php");

// obtengo valores ingresados
$nombre 						= $_POST['inputNombre'];
$descripcion 					= $_POST['inputDescripcion'];

// obtengo id (0: nuevo)
$id 							= $_GET['id'];

// valido campos
if($nombre != "" and $descripcion != ""){
	$valido = true;
}else {
	$valido = false;
}

// valido subida del logo
if (is_uploaded_file($_FILES["inputLogo"]["tmp_name"]))
{
    # verificamos el formato de la imagen
    if ($_FILES["inputLogo"]["type"]=="image/jpeg" || $_FILES["inputLogo"]["type"]=="image/pjpeg" || $_FILES["inputLogo"]["type"]=="image/gif" || $_FILES["inputLogo"]["type"]=="image/bmp" || $_FILES["inputLogo"]["type"]=="image/png")
    {
    	$conLogo = true;
    	$logo = $link->real_escape_string(file_get_contents($_FILES["inputLogo"]["tmp_name"]));

    }else{
    	$conLogo = false;
    }
}else{
	$conLogo = false;
}

// valido subida de la foto
if (is_uploaded_file($_FILES["inputFoto"]["tmp_name"]))
{
    # verificamos el formato de la imagen
    if ($_FILES["inputFoto"]["type"]=="image/jpeg" || $_FILES["inputFoto"]["type"]=="image/pjpeg" || $_FILES["inputFoto"]["type"]=="image/gif" || $_FILES["inputFoto"]["type"]=="image/bmp" || $_FILES["inputFoto"]["type"]=="image/png")
    {
    	$conFoto = true;
    	$foto = $link->real_escape_string(file_get_contents($_FILES["inputFoto"]["tmp_name"]));

    }else{
    	$conFoto = false;
    }
}else{
	$conFoto = false;
}

// condiciones
if($id=='0' and ($conFoto == false or  $conLogo == false)){
	$valido = false;
}

if ($valido){
	if($id=='0'){
		$sql = "INSERT INTO servicios_empresas (nombre, descripcion, logo, foto)
		VALUES ('".$nombre."', '".$descripcion."', '".$logo."', '".$foto."')";
	}else{
		if($conFoto){
			if($conLogo){
				$sql = "UPDATE servicios_empresas SET nombre='".$nombre."', descripcion='".$descripcion."', logo='".$logo."', foto='".$foto."' WHERE id =".$id;
			}else{
				$sql = "UPDATE servicios_empresas SET nombre='".$nombre."', descripcion='".$descripcion."', foto='".$foto."' WHERE id =".$id;
			}
						
		}else{
			if($conLogo){
				$sql = "UPDATE servicios_empresas SET nombre='".$nombre."', descripcion='".$descripcion."', logo='".$logo."' WHERE id =".$id;
			}else{
				$sql = "UPDATE servicios_empresas SET nombre='".$nombre."', descripcion='".$descripcion."' WHERE id =".$id;
			}
			
		}
	}

	if ($link->query($sql) === TRUE) {
		header("Location: /admin_new/src/empresas/listar_empresas.php?error=0");
	} else {
		if($id=="0"){
			header("Location: /admin_new/src/empresas/nuevo_empresas.php?error=1");
		}else{
			header("Location: /admin_new/src/empresas/listar_empresas.php?error=1");
		}
	}

	$link->close();
}else{
	if($id=="0"){
		header("Location: /admin_new/src/empresas/nuevo_empresas.php?error=1");
	}else{
		header("Location: /admin_new/src/empresas/listar_empresas.php?error=1");
	}
}

?>