<!DOCTYPE html>
<?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/headers/header.php"); ?>
    <body>
    <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/login/islogin.php"); ?>
    <?php $_SESSION['idPagina'] = 7; ?>

        <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/menu/menu.php"); ?>

        <section id="main">
            <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/sidebar/sidebar.php"); ?>

            <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/chat/chat.php"); ?>

            <section id="content">
                <?php
                    include($_SERVER['DOCUMENT_ROOT']."/admin_new/config/conexionSQL.php");
                    $id  = $_GET['id'];
                    $query = "select idvivo,idcategoriavideo,programa,bloque,titulo,codigo,DATE_FORMAT(fecha,'%d/%m/%Y') as fecha, entrevistado,informe from vivos where idvivo = ".$id;

                    $result1 = mysqli_query($link, $query) or die (mysql_error());

                    $datos = mysqli_fetch_array($result1)

                ?>
                <div class="container">
                    <div class="block-header">
                        <h2>Está en el panel de administración de Onlaos.com</h2>
                    </div>

                    <div class="card">
                        <div class="card-header">
                            <h2>Editar Videos en Vivo del Sistema
                                
                            </h2>
                        </div>

                        <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/alertas/alerta_nuevo_vivo.php"); ?>

                        <div class="card-body card-padding">
                            <form role="form" action=<?php echo "/admin_new/src/vivos/guardar_vivo.php?id=".$_GET['id']; ?> method="post" enctype="multipart/form-data">
                            
                                <div class="form-group">
                                    <label for="inputGaleria">Categoría</label><br>
                                    <div class="fg-line">
                                        <div class="select">
                                            <select class="form-control" id="inputCategoria" name="inputCategoria">
                                                <option>Seleccionar...</option>
                                                <?php
                                                    include($_SERVER['DOCUMENT_ROOT']."/admin_new/config/conexionSQL.php");

                                                    $query = "select idcategoria, titulo from categoriavideos";

                                                    $result = mysqli_query($link, $query) or die (mysql_error());

                                                    while($row = mysqli_fetch_array($result)) {
                                                        if($datos['idcategoriavideo']== $row['idcategoria']){
                                                            echo '<option value="'.$row['idcategoria'].'" selected>'.$row['titulo'].'</option>';
                                                        }else{
                                                            echo '<option value="'.$row['idcategoria'].'">'.$row['titulo'].'</option>';
                                                        }
                                                    }

                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group fg-line">
                                    <label for="inputPrograma">Programa</label>
                                    <input type="text" class="form-control input-sm" id="inputPrograma" name="inputPrograma"
                                           placeholder="Ingresar el programa" value=<?php echo '"'.$datos['programa'].'"' ?>>
                                </div>

                                <div class="form-group fg-line">
                                    <label for="inputBloque">Bloque</label>
                                    <input type="text" class="form-control input-sm" id="inputBloque" name="inputBloque"
                                           placeholder="Ingresar el bloque" value=<?php echo '"'.$datos['bloque'].'"' ?> >
                                </div>

                                <div class="form-group fg-line">
                                    <label for="inputTitulo">Título</label>
                                    <input type="text" class="form-control input-sm" id="inputTitulo" name="inputTitulo"
                                           placeholder="Ingresar título" value=<?php echo '"'.$datos['titulo'].'"' ?>>
                                </div>

                                <div class="form-group fg-line">
                                    <label for="inputCodigo">Código</label>
                                    <input type="text" class="form-control input-sm" id="inputCodigo" name="inputCodigo"
                                           placeholder="Ingresar código" value=<?php echo '"'.$datos['codigo'].'"' ?> >
                                </div>
                                
                                <div class="input-group form-line">
                                    <label for="inputFecha">Fecha</label>
                                    
                                    <input type="text" class="form-control date-picker" placeholder="Click aqui..." id="inputFecha" name="inputFecha"
                                            value=<?php echo '"'.$datos['fecha'].'"' ?> >
                                </div>
                                <br>

                                <div class="form-group fg-line">
                                    <label for="inputEntrevistado">Entrevistado</label>
                                    <input type="text" class="form-control input-sm" id="inputEntrevistado" name="inputEntrevistado"
                                           placeholder="Ingresar el entrevistado" value=<?php echo '"'.$datos['entrevistado'].'"' ?> >
                                </div>                                

                                <div class="form-group fg-line">
                                    <label for="inputInforme">N° de Informe</label>
                                    <input type="text" class="form-control input-sm" id="inputInforme" name="inputInforme"
                                           placeholder="Ingresar el n° de informe" value=<?php echo '"'.$datos['informe'].'"' ?> >
                                </div>

                                

                                
                                <button type="submit" class="btn btn-primary btn-sm m-t-10">Guardar</button>
                            </form>
                        </div>
                    </div>

                </div>
            </section>


        </section>

        <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/footers/footer.php"); ?>

        <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/loader/loader.php"); ?>

    </body>
  </html>
