<?php

// importo datos de conexion
include($_SERVER['DOCUMENT_ROOT']."/admin_new/config/conexionSQL.php");

// obtengo valores ingresados
$titulo 					= $_POST['inputTitulo'];

// obtengo id (0: nuevo)
$id 						= $_GET['id'];

// valido campos
if($titulo != ""){
	$valido = true;
}else {
	$valido = false;
}

// valido subida de archivo
if (is_uploaded_file($_FILES["inputFoto"]["tmp_name"]))
{
    # verificamos el formato de la imagen
    if ($_FILES["inputFoto"]["type"]=="image/jpeg" || $_FILES["inputFoto"]["type"]=="image/pjpeg" || $_FILES["inputFoto"]["type"]=="image/gif" || $_FILES["inputFoto"]["type"]=="image/bmp" || $_FILES["inputFoto"]["type"]=="image/png")
    {
    	$conFoto = true;
    	$foto = $link->real_escape_string(file_get_contents($_FILES["inputFoto"]["tmp_name"]));

    }else{
    	$conFoto = false;
    }
}else{
	$conFoto = false;
}

// condiciones
if($id=='0' and $conFoto == false){
	$valido = false;
}

if ($valido){
	if($id=='0'){
		$sql = "INSERT INTO categorias (titulo, foto)
		VALUES ('".$titulo."', '".$foto."')";
	}else{
		if($conFoto){
			
			$sql = "UPDATE categorias SET titulo='".$titulo."', foto='".$foto."' WHERE idcategoria =".$id;
			
		}else{
			
			$sql = "UPDATE categorias SET titulo='".$titulo."' WHERE idcategoria =".$id;
			
		}
	}

	if ($link->query($sql) === TRUE) {
		header("Location: /admin_new/src/categorias/listar_categoria.php?error=0");
	} else {
		if($id=="0"){
			header("Location: /admin_new/src/categorias/nuevo_categoria.php?error=1");
		}else{
			header("Location: /admin_new/src/categorias/listar_categoria.php?error=1");
		}
	}

	$link->close();
}else{
	if($id=="0"){
		header("Location: /admin_new/src/categorias/nuevo_categoria.php?error=1");
	}else{
		header("Location: /admin_new/src/categorias/listar_categoria.php?error=1");
	}
}



?>