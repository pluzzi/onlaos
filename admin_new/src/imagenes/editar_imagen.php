<!DOCTYPE html>
<?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/headers/header.php"); ?>
    <body>
    <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/login/islogin.php"); ?>
    <?php $_SESSION['idPagina'] = 8; ?>

        <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/menu/menu.php"); ?>

        <section id="main">
            <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/sidebar/sidebar.php"); ?>

            <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/chat/chat.php"); ?>

            <section id="content">
            <?php
                include($_SERVER['DOCUMENT_ROOT']."/admin_new/config/conexionSQL.php");
                $id  = $_GET['id'];
                $query = "select idimagen, foto, idgaleria from imagenes where idimagen = ".$id;

                $result1 = mysqli_query($link, $query) or die (mysql_error());

                $datos = mysqli_fetch_array($result1)

            ?>
                <div class="container">
                    <div class="block-header">
                        <h2>Está en el panel de administración de Onlaos.com</h2>
                    </div>

                    <div class="card">
                        <div class="card-header">
                            <h2>Editar Imágen del Sistema
                            </h2>
                        </div>

                        <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/alertas/alerta_nuevo_img.php"); ?>

                        <div class="card-body card-padding">
                            <form role="form" action=<?php echo "/admin_new/src/imagenes/guardar_imagen.php?id=".$_GET['id']; ?> method="post" enctype="multipart/form-data">
                                
                                <div class="form-group">
                                <label for="inputGaleria">Galería</label><br>
                                    <div class="fg-line">
                                        <div class="select">
                                            <select class="form-control" id="inputGaleria" name="inputGaleria">
                                                <option>Seleccionar...</option>
                                                <?php
                                                    include($_SERVER['DOCUMENT_ROOT']."/admin_new/config/conexionSQL.php");

                                                    $query = "select idgaleria, nombre from galerias";

                                                    $result = mysqli_query($link, $query) or die (mysql_error());

                                                    while($row = mysqli_fetch_array($result)) {
                                                        if($datos['idgaleria']== $row['idgaleria']){
                                                            echo '<option value="'.$row['idgaleria'].'" selected>'.$row['nombre'].'</option>';
                                                        }else{
                                                            echo '<option value="'.$row['idgaleria'].'">'.$row['nombre'].'</option>';
                                                        }

                                                        
                                                    }

                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group fg-line">
                                    <label for="inputFoto">Foto</label><br>
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput">
                                            <?php echo '<img src="data:image/jpeg;base64,'.base64_encode( $datos['foto'] ).'" />'; ?>
                                        </div>
                                        <div>
                                            <span class="btn btn-primary btn-file m-r-10">
                                                <span class="fileinput-new">Seleccionar imágen</span>
                                                <input type="file" id="inputFoto" name="inputFoto"  >
                                            </span>
                                            <a href="#" class="btn btn-danger fileinput-exists"
                                               data-dismiss="fileinput">Borrar</a>
                                        </div>
                                    </div>
                                </div>
                                
                                <button type="submit" class="btn btn-primary btn-sm m-t-10">Guardar</button>
                            </form>
                        </div>
                    </div>

                </div>
            </section>


        </section>

        <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/footers/footer.php"); ?>

        <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/loader/loader.php"); ?>

    </body>
  </html>
