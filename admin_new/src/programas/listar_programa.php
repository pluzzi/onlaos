<!DOCTYPE html>
<?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/headers/header.php"); ?>
    <body>
    <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/login/islogin.php"); ?>
    <?php $_SESSION['idPagina'] = 7; ?>

        <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/menu/menu.php"); ?>

        <section id="main">
            <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/sidebar/sidebar.php"); ?>

            <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/chat/chat.php"); ?>

            <section id="content">
                <div class="container">
                    <div class="block-header">
                        <h2>Está en el panel de administración de Onlaos.com</h2>
                    </div>

                    <div class="card">
                        <div class="card-header">
                            <h2>Lista de Programas
                                <small>En esta sección se podrán administrar los programas del sistema.
                                </small>

                            </h2>
                            <h1><a href="nuevo_programa.php"><i class="zmdi zmdi-file-plus"></i></a></h1>
                        </div>

                        <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/alertas/alerta_nuevo_programa.php"); ?>

                        <div class="table-responsive">
                            <table id="data-table-basic" class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Programa</th>
                                    <th>Bloque</th>
                                    <th>Fecha</th>
                                    <th>Título</th>
                                    <th>Código YouTube</th>
                                    <th>Acción</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>Id</th>
                                    <th>Programa</th>
                                    <th>Bloque</th>
                                    <th>Fecha</th>
                                    <th>Título</th>
                                    <th>Código YouTube</th>
                                    <th>Acción</th>
                                </tr>
                                </tfoot>
                                <tbody>
                                <?php
                                    include($_SERVER['DOCUMENT_ROOT']."/admin_new/config/conexionSQL.php");

                                    $query = "select idprograma, programa, bloque, DATE_FORMAT(fecha, '%d/%m/%Y') as fecha, titulo, codigo from programas";
                                    $result = mysqli_query($link, $query) or die (mysql_error());

                                    while($row = mysqli_fetch_array($result)) {
                                        echo '<tr>
                                                <td>'.$row['idprograma'].'</td>
                                                <td>'.$row['programa'].'</td>
                                                <td>'.$row['bloque'].'</td>
                                                <td>'.$row['fecha'].'</td>
                                                <td>'.$row['titulo'].'</td>
                                                <td>'.$row['codigo'].'</td>
                                                <td>
                                                   <h4>
                                                        <a href="editar_programa.php?id='.$row['idprograma'].'"><i class="zmdi zmdi-edit"></i></a>
                                                        <a onclick="confirmar('.$row['idprograma'].')" ><i class="zmdi zmdi-delete"></i></a>
                                                   </h4>
                                                </td>
                                            </tr>';

                                    }


                                ?>
                               
                            </table>
                        </div>
                    </div>

                </div>
            </section>


        </section>

        <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/footers/footer.php"); ?>

        <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/loader/loader.php"); ?>

    </body>
    <script type="text/javascript">
    //Parameter
    function confirmar(id){
        swal({   
            title: "Está seguro?",   
            text: "No podrá recuperar el programa!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonText: "Si, borrarlo!",
            cancelButtonText: "No, cancelar!",   
        }).then(function(isConfirm){
            
            if(isConfirm){
                
                swal("Borrado!", "El programa ha sido borada.", "success").then(function(){ window.location = "/admin_new/src/programas/eliminar_programa.php?id="+id; });

            }

        });

    };

</script>

  </html>
