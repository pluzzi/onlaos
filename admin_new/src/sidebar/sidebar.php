<aside id="sidebar" class="sidebar c-overflow">
    <div class="s-profile">
        <a href="" data-ma-action="profile-menu-toggle">
            <div class="sp-pic">
                <img src="/admin_new/img/demo/profile-pics/1.jpg" alt="">
            </div>

            <div class="sp-info">
                <?php echo $_SESSION['Nombre']; ?>

                <i class="zmdi zmdi-caret-down"></i>
            </div>
        </a>

        <ul class="main-menu">
        <!--
            <li>
                <a href="profile-about.html"><i class="zmdi zmdi-account"></i> View Profile</a>
            </li>
            <li>
                <a href=""><i class="zmdi zmdi-input-antenna"></i> Privacy Settings</a>
            </li>
            <li>
                <a href=""><i class="zmdi zmdi-settings"></i> Settings</a>
            </li>
        -->
            <li>
                <a href="/admin_new/src/login/logout.php"><i class="zmdi zmdi-time-restore"></i> Logout</a>
            </li>
        </ul>
    </div>

    <ul class="main-menu">
        <li <?php if($_SESSION['idPagina']==0){ echo 'class="active"'; }?> ><a href="/admin_new/src/admin.php"><i class="zmdi zmdi-home"></i> Principal</a></li>
        
        <?php
            foreach($_SESSION['Permisos'] as $i =>$key) {
                if($key==1){
                    if($_SESSION['idPagina']==1){
                        echo '<li class="active"><a href="/admin_new/src/usuarios/listar_usuario.php"><i class="zmdi zmdi-account"></i> Usuarios</a></li>';
                    }else{
                        echo '<li><a href="/admin_new/src/usuarios/listar_usuario.php"><i class="zmdi zmdi-account"></i> Usuarios</a></li>';
                    }
                }elseif ($key==2) {
                    if($_SESSION['idPagina']==2){
                        echo '<li class="active"><a href="/admin_new/src/agenda/listar_agenda.php"><i class="zmdi zmdi-assignment-account"></i> Agenda</a></li>';
                    }else{
                        echo '<li><a href="/admin_new/src/agenda/listar_agenda.php"><i class="zmdi zmdi-assignment-account"></i> Agenda</a></li>';
                    }
                    
                }elseif ($key==3) {
                    if($_SESSION['idPagina']==3){
                        echo '<li class="active"><a href="/admin_new/src/categorias/listar_categoria.php"><i class="zmdi zmdi-folder"></i> Categorías</a></li>';
                    }else{
                        echo '<li><a href="/admin_new/src/categorias/listar_categoria.php"><i class="zmdi zmdi-folder"></i> Categorías</a></li>';
                    }
                    
                }elseif ($key==4) {
                    if($_SESSION['idPagina']==4){
                        echo '<li class="active"><a href="/admin_new/src/categoria_videos/listar_video.php"><i class="zmdi zmdi-collection-video"></i> Categorías de videos</a></li>';
                    }else{
                        echo '<li><a href="/admin_new/src/categoria_videos/listar_video.php"><i class="zmdi zmdi-collection-video"></i> Categorías de videos</a></li>';
                    }
                    
                }elseif ($key==5) {
                    if($_SESSION['idPagina']==5){
                        echo '<li class="active"><a href="/admin_new/src/galeria_imagenes/listar_imagen.php"><i class="zmdi zmdi-image"></i> Galería de imágenes</a></li>';
                    }else{
                        echo '<li><a href="/admin_new/src/galeria_imagenes/listar_imagen.php"><i class="zmdi zmdi-image"></i> Galería de imágenes</a></li>';
                    }
                    if($_SESSION['idPagina']==8){
                        echo '<li class="active"><a href="/admin_new/src/imagenes/listar_imagen.php"><i class="zmdi zmdi-image"></i> Imágenes</a></li>';
                    }else{
                        echo '<li><a href="/admin_new/src/imagenes/listar_imagen.php"><i class="zmdi zmdi-image"></i> Imágenes</a></li>';
                    }
                    
                }elseif ($key==6) {
                    if($_SESSION['idPagina']==6){
                        echo '<li class="active"><a href="/admin_new/src/novedades/listar_novedad.php"><i class="zmdi zmdi-bookmark-outline"></i> Novedades</a></li>';
                    }else{
                        echo '<li><a href="/admin_new/src/novedades/listar_novedad.php"><i class="zmdi zmdi-bookmark-outline"></i> Novedades</a></li>';
                    }
                    
                }elseif ($key==7) {
                    if($_SESSION['idPagina']==7){
                        echo '<li class="active"><a href="/admin_new/src/programas/listar_programa.php"><i class="zmdi zmdi-slideshow"></i> Programas</a></li>';
                    }else{
                        echo '<li><a href="/admin_new/src/programas/listar_programa.php"><i class="zmdi zmdi-slideshow"></i> Programas</a></li>';
                    }
                    if($_SESSION['idPagina']==10){
                        echo '<li class="active"><a href="/admin_new/src/vivos/listar_vivo.php"><i class="zmdi zmdi-slideshow"></i> En Vivo</a></li>';
                    }else{
                        echo '<li><a href="/admin_new/src/vivos/listar_vivo.php"><i class="zmdi zmdi-slideshow"></i> En Vivo</a></li>';
                    }
                    
                }elseif ($key==9) {
                    echo '
                        <li><a href="/admin_new/src/empresas/listar_empresas.php"><i class="zmdi zmdi-chart"></i> Servicios y Empresas</a></li>
                        <li><a href="/admin_new/src/publicaciones/listar_publicacion.php"><i class="zmdi zmdi-chart"></i> Publicaciones Servicios y Empresas</a></li>';
                }
            }

        ?>

    </ul>
</aside>