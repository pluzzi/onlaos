<!DOCTYPE html>
<?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/headers/header.php"); ?>
    <body>
    <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/login/islogin.php"); ?>
    <?php $_SESSION['idPagina'] = 2; ?>

        <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/menu/menu.php"); ?>

        <section id="main">
            <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/sidebar/sidebar.php"); ?>

            <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/chat/chat.php"); ?>

            <section id="content">
                <div class="container">
                    <div class="block-header">
                        <h2>Está en el panel de administración de Onlaos.com</h2>
                    </div>

                    <div class="card">
                        <div class="card-header">
                            <h2>Nueva Agenda del Sistema
                                <small>Complete los datos de la nueva Agenda.
                                </small>
                            </h2>
                        </div>

                        <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/alertas/alerta_nuevo_agenda.php"); ?>

                        <div class="card-body card-padding">
                            <form role="form" action="/admin_new/src/agenda/guardar_agenda.php?id=0" method="post" enctype="multipart/form-data">
                                <div class="form-group fg-line">
                                    <label for="inputTitulo">Título</label>
                                    <input type="text" class="form-control input-sm" id="inputTitulo" name="inputTitulo"
                                           placeholder="Ingresar Título">
                                </div>
                                <div class="form-group fg-line">
                                    <label for="inputCopete">Copete</label>
                                    <input type="text" class="form-control input-sm" id="inputCopete" name="inputCopete"
                                           placeholder="Ingresar Copete">
                                </div>
                                

                                <div class="form-group fg-line">
                                    <label for="inputCuerpo">Cuerpo</label>
                                    <textarea class="html-editor" placeholder="Ingrese el cuerpo..." id="inputCuerpo" name="inputCuerpo"></textarea>
                                </div>


                                <div class="form-group fg-line">
                                    <label for="inputFoto">Foto</label><br>
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput"></div>
                                        <div>
                                            <span class="btn btn-primary btn-file m-r-10">
                                                <span class="fileinput-new">Seleccionar imágen</span>
                                                <input type="file" id="inputFoto" name="inputFoto">
                                            </span>
                                            <a href="#" class="btn btn-danger fileinput-exists"
                                               data-dismiss="fileinput">Borrar</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="input-group form-line">
                                    <label for="inputFecha">Fecha</label>
                                    
                                    <input type="text" class="form-control date-picker" placeholder="Click aqui..." id="inputFecha" name="inputFecha">
                                </div>

                                
                                <button type="submit" class="btn btn-primary btn-sm m-t-10">Guardar</button>
                            </form>
                        </div>
                    </div>

                </div>
            </section>


        </section>

        <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/footers/footer.php"); ?>

        <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/loader/loader.php"); ?>

    </body>
  </html>
