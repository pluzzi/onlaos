<?php

// importo datos de conexion
include($_SERVER['DOCUMENT_ROOT']."/admin_new/config/conexionSQL.php");

// obtengo valores ingresados
$titulo 					= $_POST['inputTitulo'];
$copete 					= $_POST['inputCopete'];
$cuerpo 	     			= $_POST['inputCuerpo'];
$fecha 			    		= $_POST['inputFecha'];

// obtengo id (0: nuevo)
$id 						= $_GET['id'];

// valido campos
if($titulo != "" and $copete != "" and $cuerpo != ""){
	$valido = true;
}else {
	$valido = false;
}

// valido subida de archivo
if (is_uploaded_file($_FILES["inputFoto"]["tmp_name"]))
{
    # verificamos el formato de la imagen
    if ($_FILES["inputFoto"]["type"]=="image/jpeg" || $_FILES["inputFoto"]["type"]=="image/pjpeg" || $_FILES["inputFoto"]["type"]=="image/gif" || $_FILES["inputFoto"]["type"]=="image/bmp" || $_FILES["inputFoto"]["type"]=="image/png")
    {
    	$conFoto = true;
    	$foto = $link->real_escape_string(file_get_contents($_FILES["inputFoto"]["tmp_name"]));

    }else{
    	$conFoto = false;
    }
}else{
	$conFoto = false;
}

// condiciones
if($id=='0' and $conFoto == false){
	$valido = false;
}

if ($valido){
	if($fecha!=""){
		$aux = explode("/", $fecha);
		$fecha = $aux[2]."-".$aux[1]."-".$aux[0];
	}
	

	if($id=='0'){
		$sql = "INSERT INTO agenda (titulo, copete, cuerpo, fechaPublicacion, foto)
		VALUES ('".$titulo."', '".$copete."', '".$cuerpo."', '".$fecha."', '".$foto."')";
	}else{
		if($conFoto){
			if($fecha==""){
				$sql = "UPDATE agenda SET titulo='".$titulo."', copete='".$copete."', cuerpo='".$cuerpo."', foto='".$foto."'
				WHERE idagenda =".$id;
			}else{
				$sql = "UPDATE agenda SET titulo='".$titulo."', copete='".$copete."', cuerpo='".$cuerpo."', fechaPublicacion='".$fecha."', foto='".$foto."' WHERE idagenda =".$id;
			}
		}else{
			if($fecha==""){
				$sql = "UPDATE agenda SET titulo='".$titulo."', copete='".$copete."', cuerpo='".$cuerpo."' WHERE idagenda =".$id;
			}else{
				$sql = "UPDATE agenda SET titulo='".$titulo."', copete='".$copete."', cuerpo='".$cuerpo."', fechaPublicacion='".$fecha."' WHERE idagenda =".$id;
			}
		}
	}

	if ($link->query($sql) === TRUE) {
		header("Location: /admin_new/src/agenda/listar_agenda.php?error=0");
	} else {
		if($id=="0"){
			header("Location: /admin_new/src/agenda/nuevo_agenda.php?error=1");
		}else{
			header("Location: /admin_new/src/agenda/listar_agenda.php?error=1");
		}
	}

	$link->close();
}else{
	if($id=="0"){
		header("Location: /admin_new/src/agenda/nuevo_agenda.php?error=1");
	}else{
		header("Location: /admin_new/src/agenda/listar_agenda.php?error=1");
	}
}



?>