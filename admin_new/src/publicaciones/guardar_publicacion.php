<?php

// importo datos de conexion
include($_SERVER['DOCUMENT_ROOT']."/admin_new/config/conexionSQL.php");

// obtengo id (0: nuevo)
$id 						= $_GET['id'];


// obtengo valores ingresados basicos
if($id == 0){
	$tipo  = $_POST['inputTipo'];
	$empresa  = $_POST['inputEmpresa'];
}else{
	$sql = "select idTipo, idEmpresa from servicios_empresas_publicaciones where id = ".$id;
	$result = mysqli_query($link, $sql) or die (mysql_error());
    $row = mysqli_fetch_array($result);

    $tipo  = $row['idTipo'];
	$empresa  = $row['idEmpresa'];
}


$titulo					= $_POST['inputTitulo'];
$copete					= $_POST['inputCopete'];
$texto					= $_POST['inputTexto'];
$video					= $_POST['inputVideo'];
$fecha					= $_POST['inputFecha'];
$orden					= $_POST['inputOrden'];

// valido subida de la foto
if (is_uploaded_file($_FILES["inputFoto"]["tmp_name"]))
{
    # verificamos el formato de la imagen
    if ($_FILES["inputFoto"]["type"]=="image/jpeg" || $_FILES["inputFoto"]["type"]=="image/pjpeg" || $_FILES["inputFoto"]["type"]=="image/gif" || $_FILES["inputFoto"]["type"]=="image/bmp" || $_FILES["inputFoto"]["type"]=="image/png")
    {
    	$foto = $link->real_escape_string(file_get_contents($_FILES["inputFoto"]["tmp_name"]));
    }

}

// validaciones
$valido = true;

if($titulo == "" or $copete == "" or $texto == ""){
	$valido = false;
}

if($tipo == 2 and $video == ""){
	$valido = false;
}

if($valido){
	if($fecha!=""){
		$aux = explode("/", $fecha);
		$fecha = $aux[2]."-".$aux[1]."-".$aux[0];
	}

	if($tipo == 1){ // Imagenes
		if($id==0){
			$sql = "INSERT INTO servicios_empresas_publicaciones (idEmpresa, idTipo, titulo, copete, texto, foto, fecha, orden)
			VALUES (".$empresa.", ".$tipo.",'".$titulo."', '".$copete."', '".$texto."', '".$foto."', '".$fecha."', '".$orden."')";
		}else{
			if($foto == ""){
				$sql = "UPDATE servicios_empresas_publicaciones SET titulo='".$titulo."', copete='".$copete."', texto='".$texto."', fecha='".$fecha."', orden='".$orden."' WHERE id=".$id;
			}else{
				$sql = "UPDATE servicios_empresas_publicaciones SET titulo='".$titulo."', copete='".$copete."', texto='".$texto."', foto='".$foto."', fecha='".$fecha."', orden='".$orden."' WHERE id=".$id;
			}
			
		}
	}

	if($tipo == 2){ // Videos
		if($id==0){
			$sql = "INSERT INTO servicios_empresas_publicaciones (idEmpresa, idTipo, titulo, copete, texto, url_video, fecha, orden)
			VALUES (".$empresa.", ".$tipo.",'".$titulo."', '".$copete."', '".$texto."', '".$video."', '".$fecha."', '".$orden."')";
		}else{
			$sql = "UPDATE servicios_empresas_publicaciones SET titulo='".$titulo."', copete='".$copete."', texto='".$texto."', url_video='".$video."', fecha='".$fecha."', orden='".$orden."' WHERE id=".$id;
		}
	}

	if($tipo == 3){ // Notas
		if($id==0){
			$sql = "INSERT INTO servicios_empresas_publicaciones (idEmpresa, idTipo, titulo, copete, texto, foto, fecha, orden)
			VALUES (".$empresa.", ".$tipo.",'".$titulo."', '".$copete."', '".$texto."', '".$foto."', '".$fecha."', '".$orden."')";
		}else{
			if($foto == ""){
				$sql = "UPDATE servicios_empresas_publicaciones SET titulo='".$titulo."', copete='".$copete."', texto='".$texto."', fecha='".$fecha."', orden='".$orden."' WHERE id=".$id;
			}else{
				$sql = "UPDATE servicios_empresas_publicaciones SET titulo='".$titulo."', copete='".$copete."', texto='".$texto."', foto='".$foto."', fecha='".$fecha."', orden='".$orden."' WHERE id=".$id;
			}
			
		}
	}

	if ($link->query($sql) === TRUE) {
		header("Location: /admin_new/src/publicaciones/listar_publicacion.php?error=0");
	} else {
		if($id=="0"){
			header("Location: /admin_new/src/publicaciones/nuevo_publicacion.php?error=1");
		}else{
			header("Location: /admin_new/src/publicaciones/listar_publicacion.php?error=1");
		}
	}

	$link->close();

}else{
	header("Location: /admin_new/src/publicaciones/nuevo_publicacion.php?error=1");
}


?>