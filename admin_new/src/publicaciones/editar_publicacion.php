<!DOCTYPE html>
<?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/headers/header.php"); ?>
    <body onload="onLoad()">
    <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/login/islogin.php"); ?>
    <?php $_SESSION['idPagina'] = 2; ?>

        <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/menu/menu.php"); ?>

        <section id="main">
            <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/sidebar/sidebar.php"); ?>

            <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/chat/chat.php"); ?>

            <section id="content">
                <?php
                    include($_SERVER['DOCUMENT_ROOT']."/admin_new/config/conexionSQL.php");
                    $id  = $_GET['id'];
                    $query = "select id, idempresa, idtipo, titulo, copete, texto, foto, url_video, orden from servicios_empresas_publicaciones where id = ".$id;

                    $result = mysqli_query($link, $query) or die (mysql_error());

                    $row = mysqli_fetch_array($result)

                ?>
                <div class="container">
                    <div class="block-header">
                        <h2>Está en el panel de administración de Onlaos.com</h2>
                    </div>

                    <div class="card">
                        <div class="card-header">
                            <h2>Editar Servicios y Empresas del Sistema - Publicaciones
                                
                            </h2>
                        </div>

                        <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/alertas/alerta_nuevo_publicacion.php"); ?>

                        <div class="card-body card-padding">
                            <form role="form" action=<?php echo "/admin_new/src/publicaciones/guardar_publicacion.php?id=".$_GET['id']; ?> method="post" enctype="multipart/form-data">
                                <div class="form-group fg-line" id="inputTituloContainer">
                                    <label for="inputTitulo">Título</label>
                                    <input type="text" class="form-control input-sm" id="inputTitulo" name="inputTitulo"
                                           placeholder="Ingresar Título" value=<?php echo '"'.$row['titulo'].'"' ?>>
                                </div>
                                <div class="form-group fg-line" id="inputCopeteContainer">
                                    <label for="inputCopete">Copete</label>
                                    <input type="text" class="form-control input-sm" id="inputCopete" name="inputCopete"
                                           placeholder="Ingresar Copete" value=<?php echo '"'.$row['copete'].'"' ?>>
                                </div>
                                <div class="form-group fg-line" id="inputTextoContainer">
                                    <label for="inputTexto">Texto</label>
                                    <textarea class="html-editor" placeholder="Ingrese el texto..." id="inputTexto" name="inputTexto">
                                        <?php echo $row['texto']; ?>
                                    </textarea>
                                </div>

                                <div class="form-group fg-line" id="inputVideoContainer">
                                    <label for="inputVideo">URL Video</label>
                                    <input type="text" class="form-control input-sm" id="inputVideo" name="inputVideo"
                                           placeholder="Ingresar URL Video" value=<?php echo '"'.$row['url_video'].'"' ?>>
                                </div>
                                
                                <div class="form-group fg-line" id="inputFotoContainer">
                                    <label for="inputFoto">Foto</label><br>
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput">
                                            <?php echo '<img src="data:image/jpeg;base64,'.base64_encode( $row['foto'] ).'" />'; ?>
                                        </div>
                                        <div>
                                            <span class="btn btn-primary btn-file m-r-10">
                                                <span class="fileinput-new">Seleccionar imágen</span>
                                                <input type="file" id="inputFoto" name="inputFoto"  >
                                            </span>
                                            <a href="#" class="btn btn-danger fileinput-exists"
                                               data-dismiss="fileinput">Borrar</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group fg-line" id="inputCopeteContainer">
                                    <label for="inputOrden">Posición</label>
                                    <input type="text" class="form-control input-sm" id="inputOrden" name="inputOrden"
                                           placeholder="Ingresar Posición" value=<?php echo '"'.$row['orden'].'"' ?>>
                                </div>


                                
                                <button type="submit" class="btn btn-primary btn-sm m-t-10" >Guardar</button>
                            </form>
                        </div>
                    </div>

                </div>
            </section>


        </section>

        <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/footers/footer.php"); ?>

        <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/loader/loader.php"); ?>

    </body>
    <script type="text/javascript">
        function onLoad(){
            document.getElementById('inputTituloContainer').style.visibility = "hidden";
            document.getElementById('inputCopeteContainer').style.visibility = "hidden";
            document.getElementById('inputTextoContainer').style.visibility = "hidden";
            document.getElementById('inputFotoContainer').style.visibility = "hidden";
            document.getElementById('inputVideoContainer').style.visibility = "hidden";
        
            var select = <?php echo $row['idtipo'] ?>
        

            if(select == 1){
                document.getElementById('inputTituloContainer').style.visibility = "visible";
                document.getElementById('inputCopeteContainer').style.visibility = "visible";
                document.getElementById('inputTextoContainer').style.visibility = "visible";
                document.getElementById('inputFotoContainer').style.visibility = "visible";
            }
            if(select == 2){
                document.getElementById('inputTituloContainer').style.visibility = "visible";
                document.getElementById('inputCopeteContainer').style.visibility = "visible";
                document.getElementById('inputTextoContainer').style.visibility = "visible";
                document.getElementById('inputVideoContainer').style.visibility = "visible";
            }
            if(select == 3){
                document.getElementById('inputTituloContainer').style.visibility = "visible";
                document.getElementById('inputCopeteContainer').style.visibility = "visible";
                document.getElementById('inputTextoContainer').style.visibility = "visible";
                document.getElementById('inputFotoContainer').style.visibility = "visible";
            }
        }


        

    </script>
  </html>
