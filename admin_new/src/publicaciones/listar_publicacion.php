<!DOCTYPE html>
<?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/headers/header.php"); ?>
    <body>
    <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/login/islogin.php"); ?>
    <?php $_SESSION['idPagina'] = 2; ?>

        <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/menu/menu.php"); ?>

        <section id="main">
            <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/sidebar/sidebar.php"); ?>

            <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/chat/chat.php"); ?>

            <section id="content">
                <div class="container">
                    <div class="block-header">
                        <h2>Está en el panel de administración de Onlaos.com</h2>
                    </div>

                    <div class="card">
                        <div class="card-header">
                            <h2>Lista de Servicios y Empresas - Publicaciones
                                <small>En esta sección se podrán administrar los servicios y empresas del sistema.
                                </small>

                            </h2>
                            <h1><a href="nuevo_publicacion.php"><i class="zmdi zmdi-file-plus"></i></a></h1>
                        </div>

                        <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/alertas/alerta_nuevo_publicacion.php"); ?>

                        <div class="table-responsive">
                            <table id="data-table-basic" class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Empresa</th>
                                    <th>Tipo</th>
                                    <th>Titulo</th>
                                    <th>Posición</th>
                                    <th>Acción</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>Id</th>
                                    <th>Empresa</th>
                                    <th>Tipo</th>
                                    <th>Titulo</th>
                                    <th>Copete</th>
                                    <th>Posición</th>
                                    <th>Acción</th>
                                </tr>
                                </tfoot>
                                <tbody>
                                <?php
                                    include($_SERVER['DOCUMENT_ROOT']."/admin_new/config/conexionSQL.php");

                                    $query = "select p.id, e.nombre as empresa, t.descripcion as tipo, p.titulo, p.copete, p.orden
                                    from servicios_empresas_publicaciones p
                                    left join tipos_publicaciones t on (p.idtipo = t.id)
                                    left join servicios_empresas e on (e.id = p.idempresa)
                                    order by p.orden";
                                    
                                    $result = mysqli_query($link, $query) or die (mysql_error());

                                    while($row = mysqli_fetch_array($result)) {
                                        echo '<tr>
                                                <td>'.$row['id'].'</td>
                                                <td>'.$row['empresa'].'</td>
                                                <td>'.$row['tipo'].'</td>
                                                <td>'.$row['titulo'].'</td>
                                                <td>'.$row['orden'].'</td>
                                                <td>
                                                   <h4>
                                                        <a href="editar_publicacion.php?id='.$row['id'].'"><i class="zmdi zmdi-edit"></i></a>
                                                        <a onclick="confirmar('.$row['id'].')" ><i class="zmdi zmdi-delete"></i></a>
                                                   </h4>
                                                </td>
                                            </tr>';

                                    }


                                ?>
                               
                            </table>
                        </div>
                    </div>

                </div>
            </section>


        </section>

        <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/footers/footer.php"); ?>

        <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/loader/loader.php"); ?>

    </body>
    <script type="text/javascript">
    //Parameter
    function confirmar(id){
        swal({   
            title: "Está seguro?",   
            text: "No podrá recuperar la publicación!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonText: "Si, borrarlo!",
            cancelButtonText: "No, cancelar!",   
        }).then(function(isConfirm){
            
            if(isConfirm){
                
                swal("Borrado!", "La publicación ha sido borada.", "success").then(function(){ window.location = "/admin_new/src/publicaciones/eliminar_publicacion.php?id="+id; });

            }

        });

    };

</script>

  </html>

