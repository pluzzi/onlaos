<!DOCTYPE html>
<?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/headers/header.php"); ?>
    <body>
    <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/login/islogin.php"); ?>
    
    <?php $_SESSION['idPagina'] = 2; ?>

        <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/menu/menu.php"); ?>

        <?php
            include($_SERVER['DOCUMENT_ROOT']."/admin_new/config/conexionSQL.php");
            $id = $_GET['id'];
            $query = "delete from servicios_empresas_publicaciones where id=".$id;
            
            $result = mysqli_query($link, $query) or die (mysqli_error($link));
            
        ?>

        <section id="main">
            <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/sidebar/sidebar.php"); ?>

            <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/chat/chat.php"); ?>

            <section id="content">
                <div class="container">
                    <div class="block-header">
                        <h2>Está en el panel de administración de Onlaos.com</h2>
                    </div>

                    <div class="card">
                        <div class="card-header">
                            <h2>Lista de Servicios y Empresas - Publicaciones
                                <small>En esta sección se podrán administrar los servicios y empresas del sistema.
                                </small>

                            </h2>
                            <br>
                            <button class="btn btn-primary waves-effect" onclick="volver()">Volver</button>
                        </div>

                        

                    </div>

                </div>
            </section>


        </section>

        <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/footers/footer.php"); ?>

        <?php include($_SERVER['DOCUMENT_ROOT']."/admin_new/src/loader/loader.php"); ?>

    </body>

  </html>


<script type="text/javascript">
    function volver(){
        window.location = "/admin_new/src/publicaciones/listar_publicacion.php";
    }
</script>